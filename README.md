## Instalare & utilizare

1. Creare proiect nou folosind nodeJS si libraria ReactJS.
Trebuie făcut setupul astfel incat orice coleg să poată instala și rula local proiectul.

2. API - se va folosi documentatia de aici https://bitbucket.org/fericiticeiprigoniti/docs/src
Actualizarile si noile endpointuri care vor fi făcute se vor semnala in changelog.md.

3. Design-ul (html+css) se afla in proiect (/resurse)


## Deploy & redeploy

Pasi pentru instaalarea node-ului, next si redeploy de proiect:

sudo su -

curl -sL https://deb.nodesource.com/setup_14.x | bash -

sudo apt install nodejs

su - {user_ssh}

cd /var/www/

rm -rf pi.cpco

git clone git@bitbucket.org:fericiticeiprigoniti/poetiiinchisorilor.git && mv poetiiinchisorilor pi.cpco

cd /var/www/pi.cpco/next-app/

npm install next

npm install

next build

next start &


## Redeploy

#close node server
ps aux | grep next | grep -v grep | awk -F " " {'print $2'} | xargs kill -9

next build

next start &
