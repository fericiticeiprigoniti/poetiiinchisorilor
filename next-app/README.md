This is a starter template for [Learn Next.js](https://nextjs.org/learn).

## Instruments and frameworks:
* Next-js (React framework)
* Less (for CSS) 
    * http://lesscss.org/
    * https://github.com/vercel/next-plugins/tree/master/packages/next-less


## App Structure:
* /components -> global components (eg: paginations, buttons, loader, bradcrumbs etc)
  * /Forms : button, inputs etc
  * /Navigation: pagination, breadcrumbs
  * /Overlays: empty state, loader, notifications, toasters etc.
* /modules/[numele_modulului]/Components/ -> here is the local components that used only in this Model
* /node_modules -> DO NOT TOUCH (auto installed via npm install from package.json)
* /pages -> public webpages. support folders
* /public -> used for global assets (js scripts, images, fonts, css)

## React Packages:
* Browser history -> https://www.npmjs.com/package/browser-history