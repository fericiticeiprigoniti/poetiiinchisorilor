import querystring from 'query-string';

class Backend {

    public static async getRequest(endpoint, params = {}) {
        const url = process.env.BACKEND_URL + endpoint + "?" + querystring.stringify(params)
        const response = await fetch(url);
        return await response.json();
    }

    public static async getPoeziiList(params = {}) {
        return await this.getRequest("poezii/list", params);
    }

    public static async getPoezie(id) {
        return await this.getRequest("poezii/poezie", {'id' : id});
    }

}

export default Backend;