import React from 'react';

import './Carousel.module.css';

class Carousel extends React.Component {

    render() {

        let props = this.props;

        return (
            <div className="slick_carousel">
                <div>
                    <ul className="items_list">
                        <li><a href="/poet/10"><img src="/images/poeti/poiet1.jpg" layout='fill' alt="poiet1"/></a></li>
                        <li><a href="/poet/11"><img src="/images/poeti/poiet2.jpg" layout='fill' alt="poiet2"/></a></li>
                        <li><a href="/poet/12"><img src="/images/poeti/poiet3.jpg" layout='fill' alt="poiet3"/></a></li>
                        <li><a href="/poet/13"><img src="/images/poeti/poiet4.jpg" layout='fill' alt="poiet4"/></a></li>
                        <li><a href="/poet/14"><img src="/images/poeti/poiet5.jpg" layout='fill' alt="poiet5"/></a></li>
                        <li><a href="/poet/15"><img src="/images/poeti/poiet6.jpg" layout='fill' alt="poiet6"/></a></li>
                        <li><a href="/poet/16"><img src="/images/poeti/poiet7.jpg" layout='fill' alt="poiet7"/></a></li>
                        <li><a href="/poet/17"><img src="/images/poeti/poiet8.jpg" layout='fill' alt="poiet8"/></a></li>
                        <li><a href="/poet/18"><img src="/images/poeti/poiet9.jpg" layout='fill' alt="poiet9"/></a></li>
                        <li><a href="/poet/19"><img src="/images/poeti/poiet10.jpg" layout='fill' alt="poiet10"/></a>
                        </li>
                        <li><a href="/poet/20"><img src="/images/poeti/poiet11.jpg" layout='fill' alt="poiet11"/></a>
                        </li>
                        <li><a href="/poet/21"><img src="/images/poeti/poiet12.jpg" layout='fill' alt="poiet12"/></a>
                        </li>
                        <li><a href="/poet/22"><img src="/images/poeti/poiet13.jpg" layout='fill' alt="poiet13"/></a>
                        </li>
                        <li><a href="/poet/22"><img src="/images/poeti/poiet14.jpg" layout='fill' alt="poiet14"/></a>
                        </li>
                        <li><a href="/poet/23"><img src="/images/poeti/poiet15.jpg" layout='fill' alt="poiet15"/></a>
                        </li>
                        <li><a href="/poet/24"><img src="/images/poeti/poiet16.jpg" layout='fill' alt="poiet16"/></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <ul className="items_list">
                        <li><a href="/poet/25"><img src="/images/poeti/poiet1.jpg" layout='fill' alt="poiet1"/></a></li>
                        <li><a href="/poet/26"><img src="/images/poeti/poiet2.jpg" layout='fill' alt="poiet2"/></a></li>
                        <li><a href="/poet/27"><img src="/images/poeti/poiet3.jpg" layout='fill' alt="poiet3"/></a></li>
                        <li><a href="/poet/28"><img src="/images/poeti/poiet4.jpg" layout='fill' alt="poiet4"/></a></li>
                        <li><a href="/poet/29"><img src="/images/poeti/poiet5.jpg" layout='fill' alt="poiet5"/></a></li>
                        <li><a href="/poet/30"><img src="/images/poeti/poiet6.jpg" layout='fill' alt="poiet6"/></a></li>
                        <li><a href="/poet/31"><img src="/images/poeti/poiet7.jpg" layout='fill' alt="poiet7"/></a></li>
                        <li><a href="/poet/32"><img src="/images/poeti/poiet8.jpg" layout='fill' alt="poiet8"/></a></li>
                        <li><a href="/poet/33"><img src="/images/poeti/poiet9.jpg" layout='fill' alt="poiet9"/></a></li>
                        <li><a href="/poet/34"><img src="/images/poeti/poiet10.jpg" layout='fill' alt="poiet10"/></a>
                        </li>
                        <li><a href="/poet/35"><img src="/images/poeti/poiet11.jpg" layout='fill' alt="poiet11"/></a>
                        </li>
                        <li><a href="/poet/36"><img src="/images/poeti/poiet12.jpg" layout='fill' alt="poiet12"/></a>
                        </li>
                        <li><a href="/poet/37"><img src="/images/poeti/poiet13.jpg" layout='fill' alt="poiet13"/></a>
                        </li>
                        <li><a href="/poet/10"><img src="/images/poeti/poiet14.jpg" layout='fill' alt="poiet14"/></a>
                        </li>
                        <li><a href="/poet/10"><img src="/images/poeti/poiet15.jpg" layout='fill' alt="poiet15"/></a>
                        </li>
                        <li><a href="/poet/10"><img src="/images/poeti/poiet16.jpg" layout='fill' alt="poiet16"/></a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

Carousel.propTypes = {
    //children: PropTypes.node,
};

Carousel.defaultProps = {
    //children: '',
};

export default Carousel;
