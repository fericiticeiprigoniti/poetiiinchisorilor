import React from 'react';
import Link from "next/link";
import './SecondaryNavbar.module.css';

class SecondaryNavbar extends React.Component {

	render() {

		let props = this.props;

		return (
			<ul className="hover_dropdown custom_dropdown flex_spaceb full_custom_dropdown">
				<li className="center">
					{/*todo aici este o problema*/}
					{/*href="/categorie/1"*/}
					<a className="angle_down">
						Poetii
						<i className="fa fa-caret-down" aria-hidden="true"></i>
					</a>
					<ul className="custom_dropdown_list left">
						<li><a href="/poets">Poeti</a></li>
						<li><a href="/poets/787">Fisa biografica</a></li>
						<li><a href="/fisa-carcerala/787">Fisa carcerala</a></li>
						<li><a href="/filtered-poems">Cauta poezii</a></li>
						<li><a href="/videoteca">Videoteca</a></li>
						<li><a href="/search-results">Rezultate</a></li>
						<li><a href="/biografie/787/">Biografie poet temp</a></li>

						<li><a href="/poezii-poet/787">Poezii poet temp</a></li>

						<li><a href="/poezii-filtrate">Poezii filtrate</a></li>
						<li><a href="/tematica-poeziei-carcerale/">Tematica poeziei carcerale</a></li>
						<li><a href="/tematica-poeziei-carcerale/tematica-poeziei-aleasa">Tematica poeziei aleasa</a></li>

						<li><a href="/test/787">Component</a></li>
						<li><a href="/categorie-articole">Categorie articole</a></li>
						<li><a href="/pagina-articol">Articol</a></li>

						{/*<li>*/}
						{/*	<Link href="/poezii-poet/787">*/}
						{/*		<a>Poezii poet temp</a>*/}
						{/*	</Link>*/}
						{/*</li>*/}
						<li><a href="/web/">TEST</a></li>
						<li><a href="/fcp/">TEST</a></li>

					</ul>
				</li>
				<li className="center">
					<a className="angle_down" href="/">
						Poezia
						<i className="fa fa-caret-down" aria-hidden="true"></i>
					</a>
					<ul className="custom_dropdown_list left">
						<li><a href="/poets">În temniță și lagăre</a></li>
						<li><a href="/web/">După eliberare</a></li>
						<li><a href="/">În rezistența din munți</a></li>
						<li><a href="/">În exil</a></li>
						<li><a href="/">În deportarea siberiană</a></li>
						<li><a href="/">Înainte de arestare</a></li>
					</ul>
				</li>
				<li className="center">
					<a className="angle_down" href="/">
						Momentul creatiei
						<i className="fa fa-caret-down" aria-hidden="true"></i>
					</a>
					<ul className="custom_dropdown_list left">
						<li><a href="/">În temniță și lagăre</a></li>
						<li><a href="/">După eliberare</a></li>
						<li><a href="/">În rezistența din munți</a></li>
						<li><a href="/">În exil</a></li>
						<li><a href="/">În deportarea siberiană</a></li>
						<li><a href="/">Înainte de arestare</a></li>
					</ul>
				</li>
				<li className="center">
					<a className="angle_down" href="/">Videoteca</a>
				</li>
				<li className="center">
					<a className="angle_down" href="/">
						In prezent
						<i className="fa fa-caret-down" aria-hidden="true"></i>
					</a>
					<ul className="custom_dropdown_list left">
						<li><a href="/poets/787">Poet. Fisa biografica</a></li>
						<li><a href="/fisa-carcerala/787">Poet. Fisa carcerala</a></li>
						<li><a href="/intinerariu-detentie/787">Poet. Itinerariu detentie</a></li>
						<li><a href="/biografie/787/">Poet. Biografie poet</a></li>
						<li><a href="/poezii-poet/787">Poet. Poezii poet</a></li>
						<li><a href="/poezii-poet/787">Poet. Poezie</a></li>
						<li><a href="/poezie">Poezie</a></li>
						<li><a href="/tematica-poeziei-carcerale/">Tematica poeziei carcerale</a></li>
						<li><a href="/tematica-poeziei-carcerale/tematica-poeziei-aleasa">Tematica poeziei aleasa</a></li>
						<li><a href="/pagina-articol">Articol</a></li>
						<li><a href="/pagina-articol">Articol simplu</a></li>
						<li><a href="/categorie-articole">Categorie de articole</a></li>
						<li><a href="/poeti/n">Poetii pe litere</a></li>
						<li><a href="/poets">Poetii pe litere total</a></li>
						<li><a href="/search-results">Pagina de search</a></li>
						<li><a href="/poezii-filtrate">Pagina filtrata poezii</a></li>
						<li><a href="/videoteca">Videoteca</a></li>
						<li><a href="/bibliografie">Bibliografie</a></li>

					</ul>
				</li>
			</ul>
		);
	}
}

SecondaryNavbar.propTypes = {
	//children: PropTypes.node,
};

SecondaryNavbar.defaultProps = {
	//children: '',
};

export default SecondaryNavbar;
