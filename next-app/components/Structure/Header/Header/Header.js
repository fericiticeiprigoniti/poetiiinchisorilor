import React from 'react';

import './Header.module.css';
import AlphabeticNavigation from "../AlphabeticNavigation";
import Carousel from "../Carousel";
import TopNavbar from "../TopNavbar";
import TopSearch from "../TopSearch";
import SecondaryNavbar from "../SecondaryNavbar";

class Header extends React.Component {

    render() {

        let props = this.props;

        return (
            <div>

                <div className="loading_container">
                    <div className="loading"></div>
                </div>

                <header>
                    <div className="mt25 mob_remove_mg">
                        <div className="main_container mob_remove_padd">
                            <div className="two" style={{margin: 0}}>
                                <div className="logop">
                                    <a href="/">
                                        <img className="max-width" layout='fill' src="/images/logo_pi_header.png"  alt="logo"/>
                                    </a>
                                </div>
                            </div>

                            <div className="three">
                                <div className="flex_startp width100 pos_rel disp_block mb30">
                                    <TopSearch></TopSearch>
                                    <TopNavbar></TopNavbar>
                                    <p className="fcp"><a className="uppercase" href="https://www.fericiticeiprigoniti.net/" target={"_blank"}>fcp</a></p>
                                </div>
                                <div className="set_tablet_max_width">
                                    <div className="mob_logo">
                                        <a href="/">
                                            <img className="max-width" src="/images/logo.jpg" layout='fill' alt="logo"/>
                                        </a>
                                    </div>
                                    <div className="right_col">
                                        <Carousel></Carousel>
                                        <AlphabeticNavigation></AlphabeticNavigation>
                                    </div>
                                </div>
                            </div>

                            <div className="clone_class mt10 mb30">
                                <SecondaryNavbar></SecondaryNavbar>
                            </div>

                        </div>
                    </div>
                </header>


            </div>
        );
    }
}

Header.propTypes = {
    //children: PropTypes.node,
};

Header.defaultProps = {
    //children: '',
};

export default Header;
