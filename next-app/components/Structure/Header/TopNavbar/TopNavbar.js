import React from 'react';

import './TopNavbar.module.css';

class TopNavbar extends React.Component {

    render() {

        let props = this.props;

        return (
            <nav className="nav_widthp">
                <ul className="hover_dropdown list_style">
                    <li className="angle_down">
                        <a href="/">
                            Despre proiect
                            <i className="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                        <ul className="dropdown_list">
                            <li className="active_link"><a href="/">Povestea proiectului</a>
                            </li>
                            <li><a href="/">echipa noastră</a></li>
                            <li><a href="/">drepturi de preluare</a></li>
                        </ul>
                    </li>
                    <li className="angle_down">
                        <a href="/bibliografie">
                            Bibliografie
                            <i className="fa fa-caret-down" aria-hidden="true"></i>
                        </a>
                        <ul className="dropdown_list">
                            <li className="active_link"><a href="/">Povestea proiectului</a>
                            </li>
                            <li><a href="/">echipa noastră</a></li>
                            <li><a href="/">drepturi de preluare</a></li>
                        </ul>
                    </li>
                    <li><a href="/">Sustine</a></li>
                    <li><a href="/">Contact</a></li>
                </ul>
            </nav>
        );
    }
}

TopNavbar.propTypes = {
    //children: PropTypes.node,
};

TopNavbar.defaultProps = {
    //children: '',
};

export default TopNavbar;
