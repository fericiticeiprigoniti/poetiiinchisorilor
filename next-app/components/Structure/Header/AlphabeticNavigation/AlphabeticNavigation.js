import React from 'react';

import './AlphabeticNavigation.module.css';

class AlphabeticNavigation extends React.Component {

    render() {

        let props = this.props;

        let letters = [];
        let start = 'a'.charCodeAt(0);
        let last  = 'z'.charCodeAt(0);
        for (var i = start; i <= last; ++i) {
            letters.push(String.fromCharCode(i));
        }

        const letterListItems = letters.map(function (letter) {
            return <li key={letter}><a href={"/poeti/" + letter}>{letter}</a></li>
        });

        return (
            <div className="alphabetic mb20">
                <span className="alphabetic_span red">Indice alfabetic</span>
                <ul className="alphabetic_list">
                    {letterListItems}
                </ul>
            </div>
        );
    }
}

AlphabeticNavigation.propTypes = {
    //children: PropTypes.node,
};

AlphabeticNavigation.defaultProps = {
    //children: '',
};

export default AlphabeticNavigation;
