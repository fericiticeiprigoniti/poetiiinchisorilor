import React from 'react';

import './TopSearch.module.css';

class TopSearch extends React.Component {

    render() {

        let props = this.props;

        return (
            <div className="flex_startp searchp">
                <button type="button" className="hide_md menu_mobile" aria-label="Hide menu"></button>
                <div className="searchp_form">
                    <form method="POST">
                        <div className="flex_centerp active_input">
                            <input type="text" name="input-search" className="input_searchp"
                                   placeholder="Caută o poezie sau un poet" aria-label="Caută o poezie sau un poet"/>
                            <button type="submit" className="button_searchp" aria-label="Search"></button>
                        </div>
                    </form>
                </div>
                <div className="logo_mobie">
                    <a href="/">
                        <img className="max-width" src="/images/logo-mobile.png" layout='fill'
                             alt="logo"/>
                    </a>
                </div>
            </div>
        );
    }
}

TopSearch.propTypes = {
    //children: PropTypes.node,
};

TopSearch.defaultProps = {
    //children: '',
};

export default TopSearch;
