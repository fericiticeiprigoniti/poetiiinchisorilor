import React from 'react';

import './Footer.module.css';

class Footer extends React.Component {

	render() {
		let props = this.props;

		return (
			<footer>
				<div className="main_container">
					<div className="content_footer">
						<div className="footer_main_logo font17 semibold relative">
							<a href="#"><img src="/images/logo_pi_footer.png" layout="fill" style={{width: '450px', height: '117px'}}
											 alt="Asociatia Ortodoxia Tinerilor"/></a>
							{/*<p>Un proiect al <span className="silver">Asociatiei Ortodoxia Tinerilor</span></p>*/}
						</div>
						<div className="footer_second_logo">
							<a href="#"><img src="/images/fcp_logo.jpg" style={{width: '193px'}}
											 alt="fericiti cei prigoniti" layout="fill"/></a>
						</div>
						<div className="footer_contact font17">
							<ul className="footer_contact_net mb20">
								<li><a href="#" className="fb" aria-label="Facebook"><i className="fa fa-facebook"
																						aria-hidden="true"></i></a>
								</li>
								<li><a href="#" aria-label="Youtube"><i className="fa fa-youtube"
																		aria-hidden="true"></i></a></li>
								<li><a href="#" aria-label="Donate with Paypal"><i className="fa fa-paypal"
																				   aria-hidden="true"></i></a></li>
								<li><a href="#" aria-label="RSS"><i className="fa fa-rss" aria-hidden="true"></i></a>
								</li>
							</ul>
							<ul>
								<li><a href="#" className="footer_link">Despre proiect</a></li>
								<li><a href="/bibliografie" className="footer_link">Bibliografie</a></li>
								<li><a href="#" className="footer_link">Sustine</a></li>
								<li><a href="#" className="footer_link">Contact</a></li>
							</ul>
						</div>
					</div>
				</div>
				<style jsx>{`
              
				`}</style>
			</footer>
		);
	}
}

Footer.propTypes = {
	//children: PropTypes.node,
};

Footer.defaultProps = {
	//children: '',
};

export default Footer;
