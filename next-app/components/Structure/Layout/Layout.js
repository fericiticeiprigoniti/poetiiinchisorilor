import Head from 'next/head'
import './Layout.module.css';
import Header from "../Header/Header";
import Footer from "../Footer";

export default function Layout(props) {
    return (
        <div>

            {/*Next JS Component used for filling html head tag with stuff*/}
            <Head>
                <title>{props.title}</title>
                <meta name="description" content={props.description}/>
            </Head>

            {/*Site Header*/}
            <Header></Header>

            {/*Site Contents, props.childrens are all elements placed inside <Layout></Layout>*/}
            <main>{props.children}</main>

            {/*Site Footer*/}
            <Footer></Footer>

            {/*Scripts*/}
            <script type="text/javascript" src="/js/jquery-3.2.1.min.js"></script>
            <script type="text/javascript" src="/js/slick.min.js"></script>
            <script type="text/javascript" src="/js/main.js"></script>

        </div>
    )
}
