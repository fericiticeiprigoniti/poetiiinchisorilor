import React from 'react';

import './AlphabeticNavigation.module.css';

class AlphabeticNavigation extends React.Component {

    //TODO find a way to merge with Header/AlphabeticNavigation

    render() {

        let props = this.props;

        let letters = [];
        let start = 'a'.charCodeAt(0);
        let last  = 'z'.charCodeAt(0);
        for (var i = start; i <= last; ++i) {
            letters.push(String.fromCharCode(i));
        }

        const letterListItems = letters.map(function (letter) {
            return <li key={letter}><a href={"/poeti/" + letter}>{letter}</a></li>
        });

        return (
            <div className="mb20 letter_alphabetic center mt25 relative">
                <span className="font19 alphabetic_span_bottom">Indice alfabetic</span>
                <ul className="uppercase ml5 alphabetic_list_bottom">
                    {letterListItems}
                </ul>
            </div>
        );
    }
}

AlphabeticNavigation.propTypes = {
    //children: PropTypes.node,
};

AlphabeticNavigation.defaultProps = {
    //children: '',
};

export default AlphabeticNavigation;
