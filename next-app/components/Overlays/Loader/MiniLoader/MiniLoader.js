import React from 'react';

import './MiniLoader.less';
import PropTypes from 'prop-types';

const MiniLoader = ({ color }) => <div className={`miniloader miniloader-${color}`}>loading ... </div>;
MiniLoader.propTypes = {
  /* the color of the dots */
  color: PropTypes.oneOf(['white', 'blue', 'green']),
};

MiniLoader.defaultProps = {
  color: 'white',
};

export default MiniLoader;
