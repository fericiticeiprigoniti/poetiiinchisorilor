// import {useEffect} from "react";
import {useRouter} from "next/router";
import Link from "next/link";

const index = (props) => {
    let {poets} = props;
    console.log(poets)
    const router = useRouter();
    const {id}= router.query;
    console.log({id})
    return <ul>
        {poets.map((poet)=>{
            let url = `/web/${poet.id}`;
            return <li key={poet.id} style={{padding: '1rem', margin:"1rem"}} >
                <Link href={url}>
                    <a >{poet.title}</a>
                </Link>
                </li>
        })}
    </ul>
}

export async function getStaticProps() {
    let url = 'https://jsonplaceholder.typicode.com/todos'
    const res = await fetch(url);
    const obj = await res.json();
    return {
        props: {
            // poets: obj.data
            poets: obj
        }
    }
}

export default index;

