import {useRouter} from "next/router";
// creez cele 10 pagini
export const getStaticPaths = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/todos');
    const obj = await res.json();
    const paths = obj.map((el) => {
        return {
            params: {id: el.id.toString()}
        }
    })
    return {
        paths: paths,
        fallback: false
    }
}
//  info individual
export const getStaticProps = async (context) => {
    const id = context.params.id;
    const res = await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`);
    const data = await res.json();
    return {
        props: {
            poet: data
        },
        revalidate: 60
    }
}
const detailsPoets = ({poet}) => {
    const router = useRouter();
    const {id} = router.query;
    return <div>
        <h1>{id}</h1>
        <p>{poet.title}</p>
        {/*<p>{info.title}</p>*/}
    </div>

}
export default detailsPoets;


//     const [info, setInfo] = useState([])
//
// useEffect(() => {
//     fetch('https://jsonplaceholder.typicode.com/todos', {
//         mode: 'no-cors',
//         credentials: 'include',
//         method: 'GET',
//         headers: {
//             'Content-Type': 'application/json'
//         }
//     })
//         .then(res => res.json())
//         .then(data => {
//             console.log(typeof data)
//             setInfo(data)
//         })
//         .catch((err) => {
//             console.log(err.message)
//         })
// }, [])
// console.log(info)
//
