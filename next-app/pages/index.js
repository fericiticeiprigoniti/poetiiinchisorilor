import Layout from '../components/Structure/Layout'

export default function Home() {
  return (
      <div className="container">

        <Layout title="Poeții închisorilor">

          <div className="main_container">
            <div className="testimonal_section flex_space_between pt25 pb25 mb35">
              <div className="max_width_400 center red font34">
                <p>Virgil <span className="uppercase">Mateiaș</span></p>
              </div>
              <div className="max_width_690 font20 quotation_mark relative pl50 pr60">
                <p>Poezia de detenție cu tematică carcerală nu este altceva decât o istorie în versuri, care a fost mai
                  ușor de ascuns de vigilența paznicilor, de barbaria perchezițiilor, de suspiciunea anchetatorilor și
                  deci, mai lesne de adus la lumină.</p>
              </div>
            </div>

            <div className="poem_section">
              <div className="pl40 pr35 pt10 pb25 mb35 border_left_gray child_poem_section relative">
                <div className="content_poem">
                  <div className="uppercase red semibold font15 mb15">
                    <p>Radu Gyr</p>
                  </div>
                  <div className="semibold font29 mb20">
                    {/*<p>Știu că sunt lut</p>*/}
                    {/*modificat si facut link  culoare temporara*/}
                    <a href="/poezie/10" style={{color: "black"}}>Știu că sunt lut</a>
                  </div>
                  <div className="poem_content">
                    <p>Ştiu că sunt lut, dar nu ştiu cine
                      mi-a pus în piept albine stranii,
                      să-mi scoată mir din mărăcine,
                      din bălării, împărtăşanii.</p>
                    <p>Minunea mea se-ntâmplă vie
                      din mofturi mici şi biete fleacuri…
                      Un colţ de-al meu e-o-mpărăţie,
                      o zi de-a mea răsfrânge veacuri.</p>
                    <p>De nu strâng mările în braţe,
                      prin câte-o baltă tot deretic
                      și gheara mea tot stă să-nhaţe
                      din orice taină câte-un petic.</p>
                    <p>Și dacă-ncălecat pe-o rază
                      ea nu mă urcă-n cer, încalțe</p>
                  </div>
                </div>
                <div className="read_more absolute">
                  <a href="/poezie/10">Citește mai mult</a>
                </div>
              </div>
              <div className="pl40 pr35 pt10 pb25 mb35 border_left_gray child_poem_section relative">
                <div className="content_poem">
                  <div className="uppercase red semibold font15 mb15">
                    <p>nichifor crainic</p>
                  </div>
                  <div className="semibold font29 mb20">
                    {/*<p>Unde sunt cei care nu mai sunt</p>*/}
                    {/*modificat si facut link  culoare temporara*/}
                    <a href="/poezie/10" style={{color: "black"}}>Unde sunt cei care nu mai sunt</a>
                  </div>
                  <div className="poem_content">
                    <p>Întrebat-am vîntul, zburătorul
                      Bidiviu pe care-aleargă norul
                      Către-albastre margini de pămînt:
                      Unde sînt cei care nu mai sînt?
                      Unde sînt cei care nu mai sînt?</p>
                    <p>Zis-a vîntul: Aripile lor
                      Mă doboară nevăzute-n zbor.</p>
                    <p>întrebat-am luminata ciocîrlie,
                      Candelă ce leagănă-n tărie
                      Untdelemnul cîntecului sfînt:
                      Unde sînt cei care nu mai sînt?
                      Unde sînt cei care nu mai sînt?</p>
                    <p>Zis-a ciocîrlia: S-au ascuns
                      În lumina celui nepătruns.</p>
                  </div>
                </div>
                <div className="read_more absolute">
                  <a href="/poezie/10">Citește mai mult</a>
                </div>
              </div>
              <div className="pl40 pr35 pt10 pb25 mb35 border_left_gray child_poem_section relative">
                <div className="content_poem">
                  <div className="uppercase red semibold font15 mb15">
                    <p>traian dorz</p>
                  </div>
                  <div className="semibold font29 mb20">
                    {/*							<p>Blândul Păstor</p>*/}
                    {/*modificat si facut link  culoare temporara*/}
                    <a href="/poezie/10" style={{color: "black"}}>Blândul Păstor</a>
                  </div>
                  <div className="poem_content">
                    <p>Odată L-am văzut trecând
                      Cu turma pe Păstorul Blând.
                      Mergea cu turma la izvor,
                      Blândul Păstor, Blândul Păstor.</p>
                    <p>Pe-o oaie ce căzuse jos,
                      A ridicat-o El frumos;
                      Şi-a dus-o-n braţe iubitor
                      Blândul Păstor, Blândul Păstor.</p>
                    <p>Pe alta care la pământ
                      Zăcea cu picioruşul frânt,
                      El o lega mângâietor
                      Blândul Păstor, Blândul Păstor.</p>
                    <p>Iar mai târziu L-am întâlnit,
                      Cu spini era împodobit
                      Într-o mulţime de popor</p>
                  </div>
                </div>
                <div className="read_more absolute">
                  <a href="/poezie/10">Citește mai mult</a>
                </div>
              </div>
            </div>

            <div className="events_section mb35">
              <div className="event_title uppercase font19">
                <p>Evenimente</p>
              </div>
              <div className="content_evens flex_start_between">
                <div className="content_evens_child">
                  <div className="mb35">
                    <img src="/images/events.jpg" alt="events"/>
                  </div>
                  <div className="semibold font20 text_content_event">
                    <p>Transfigurarea suferinței prin rugăciune şi poezie - Mesajul Patriarhului Daniel la Colocviul de
                      literatură creştină</p>
                  </div>
                  <div className="red read_more_red">
                    <a href="#" className="red">citeste mai departe</a>
                  </div>
                </div>
                <div className="content_evens_child">
                  <div className="mb35">
                    <img src="/images/events.jpg" alt="events"/>
                  </div>
                  <div className="semibold font20 text_content_event">
                    <p>Transfigurarea suferinței prin rugăciune şi poezie - Mesajul Patriarhului Daniel la Colocviul de
                      literatură creştină</p>
                  </div>
                  <div className="red read_more_red">
                    <a href="#" className="red">citeste mai departe</a>
                  </div>
                </div>
                <div className="content_evens_child">
                  <div className="mb35">
                    <img src="/images/events.jpg" alt="events"/>
                  </div>
                  <div className="semibold font20 text_content_event">
                    <p>Transfigurarea suferinței prin rugăciune şi poezie - Mesajul Patriarhului Daniel la Colocviul de
                      literatură creştină Transfigurarea suferinței prin rugăciune şi poezie - Mesajul Patriarhului
                      Daniel
                      la Colocviul de literatură creştină</p>
                  </div>
                  <div className="red read_more_red">
                    <a href="#" className="red">citeste mai departe</a>
                  </div>
                </div>
              </div>
            </div>

            <hr className="red_hr"/>

            <div className="syntheses_section flex_start_between relative">
              <div className="syntheses_left pl40 pt50">
                <div className="uppercase font43 mb100 zindex10 relative">
                  <p><span className="red">Fenomenul</span> poeziei carcerale</p>
                </div>
                <div className="mb100 font23 mobile_hidden short_poem zindex10 relative">
                  <p className="relative" style={{display: "inline-block"}}>
                    Ne vom întoarce într-o zi,<br/>
                    Ne vom întoarce neapărat.<br/>
                    Vor fi apusuri aurii,<br/>
                    Cum au mai fost cînd am plecat.
                  </p>
                </div>
              </div>

              <div className="syntheses_right">
                <div className="content_syntheses_right mt15 pt10 flex_start_between">
                  <div className="img_syntheses_right center">
                    <img src="/images/sinteza.png" alt=""/>
                  </div>
                  <div className="txt_syntheses_right ml20">
                    <div className="font23 semibold mb20 title_txt_right">
                      {/*culoare temporara pana se aproba*/}
                      <a href="/articol/20" style={{color: "black"}}>În ce condiții s-a născut poezia din
                        închisori</a>
                    </div>
                    <div className="gray font17 details_txt_right">
                      <p>În libertate timpul este un dar preţios care îţi permite să faci tot ce doreşti. În închisoare
                        timpul... </p>
                    </div>
                  </div>
                </div>

                <div className="content_syntheses_right mt15 pt10 flex_start_between">
                  <div className="img_syntheses_right center">
                    <img src="/images/sinteza.png" alt=""/>
                  </div>
                  <div className="txt_syntheses_right ml20">
                    <div className="font23 semibold mb20 title_txt_right">
                      {/*culoare temporara pana se aproba*/}
                      <a href="/articol/10" style={{color: "black"}}>În ce condiții s-a născut poezia din
                        închisori</a>
                    </div>
                    <div className="gray font17 details_txt_right">
                      <p>În libertate timpul este un dar preţios care îţi permite să faci tot ce doreşti. În închisoare
                        timpul... </p>
                    </div>
                  </div>
                </div>

                <div className="flex_start font23 semibold mt15 pt10 more_articles mobile_block">
                  <p>Circularea poeziilor prin închisori <span className="red">&raquo;</span></p>
                  <p>Ce au reprezentat poetii si poezia lor pentru patimitorii din inchisori <span
                      className="red">&raquo;</span></p>
                </div>

              </div>

              <div className="absolute more_syntheses flex_start_between">
                <div className="triangle_red"></div>
                <div className="more_syntheses_bg">
                  <a href="#" className="uppercase">Vezi toate sintezele</a>
                </div>
              </div>
            </div>

            <hr className="gray_hr"/>

            <div className="thematic_poems">
              <div className="title_thematic_poems semibold uppercase center font32 pb50 pt30">
                <p>Poezii cu subiectul <span className="red">foamea</span></p>
              </div>
              <div className="content_thematic_poems">
                <div className="pl40 pr35 pt10 pb25 mb35 border_left_gray child_poem_section relative">
                  <div className="content_poem">
                    <div className="uppercase red semibold font15 mb15">
                      <p>Radu Gyr</p>
                    </div>
                    <div className="semibold font29 mb20">
                      <p>Știu că sunt lut</p>
                    </div>
                    <div className="poem_content">
                      <p>Ştiu că sunt lut, dar nu ştiu cine
                        mi-a pus în piept albine stranii,
                        să-mi scoată mir din mărăcine,
                        din bălării, împărtăşanii.</p>
                      <p>Minunea mea se-ntâmplă vie
                        din mofturi mici şi biete fleacuri…
                        Un colţ de-al meu e-o-mpărăţie,
                        o zi de-a mea răsfrânge veacuri.</p>
                      <p>De nu strâng mările în braţe,
                        prin câte-o baltă tot deretic
                        și gheara mea tot stă să-nhaţe
                        din orice taină câte-un petic.</p>
                      <p>Și dacă-ncălecat pe-o rază
                        ea nu mă urcă-n cer, încalțe</p>
                    </div>
                  </div>
                  <div className="read_more absolute">
                    <a href="#">Citeste mai mult</a>
                  </div>
                </div>
                <div className="pl40 pr35 pt10 pb25 mb35 border_left_gray child_poem_section relative">
                  <div className="content_poem">
                    <div className="uppercase red semibold font15 mb15">
                      <p>Radu Gyr</p>
                    </div>
                    <div className="semibold font29 mb20">
                      <p>Știu că sunt lut</p>
                    </div>
                    <div className="poem_content">
                      <p>Ştiu că sunt lut, dar nu ştiu cine
                        mi-a pus în piept albine stranii,
                        să-mi scoată mir din mărăcine,
                        din bălării, împărtăşanii.</p>
                      <p>Minunea mea se-ntâmplă vie
                        din mofturi mici şi biete fleacuri…
                        Un colţ de-al meu e-o-mpărăţie,
                        o zi de-a mea răsfrânge veacuri.</p>
                      <p>De nu strâng mările în braţe,
                        prin câte-o baltă tot deretic
                        și gheara mea tot stă să-nhaţe
                        din orice taină câte-un petic.</p>
                      <p>Și dacă-ncălecat pe-o rază
                        ea nu mă urcă-n cer, încalțe</p>
                    </div>
                  </div>
                  <div className="read_more absolute">
                    <a href="#">Citeste mai mult</a>
                  </div>
                </div>
                <div className="pl40 pr35 pt10 pb25 mb35 border_left_darkgray child_poem_section relative">
                  <div className="uppercase font15 semibold mb35">
                    <span className="red font23 title_lspacing">Tematica</span><br/> poeziei carcerale
                  </div>
                  <ul className="thematic_list mb20">
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                    <li>bucuria</li>
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                    <li>bucuria</li>
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                    <li>bucuria</li>
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                    <li>bucuria</li>
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                    <li>bucuria</li>
                    <li>Adevărul</li>
                    <li>pribegia</li>
                    <li>prietenia</li>
                  </ul>
                  <div className="see_all_thematic absolute">
                    <a href="#">Vezi toate subiectele &raquo;</a>
                  </div>
                </div>
              </div>
            </div>

            <div className="video_section flex_space_between mb35">
              <div className="uppercase font32 title_video_section">
                <p>videoteca</p>
              </div>
              <div className="video_section_list">
                <ul>
                  <li><a href="#">conferinte</a></li>
                  <li><a href="#">concerte</a></li>
                  <li><a href="#">documentare</a></li>
                </ul>
              </div>
            </div>

            <div className="content_video_section mb100">
              <div className="pl40 pr35 pt10 pb25 border_left_gray child_poem_section">
                <div className="center">
                  <a href="#">
                    <img src="/images/video1.jpg" alt=""/>
                  </a>
                </div>
                <div className="semibold red mt15 uppercase font15">
                  <a href="#" className="red">video</a>
                </div>
                <div className="font29 video_desc semibold mt15">
                  <p>Cu Iisus in celula (concert Tudor Gheorghe)</p>
                </div>
              </div>

              <div className="pl40 pr35 pt10 pb25 border_left_gray child_poem_section">
                <div className="center">
                  <a href="#">
                    <img src="/images/video1.jpg" alt=""/>
                  </a>
                </div>
                <div className="semibold red mt15 uppercase font15">
                  <a href="#" className="red">video</a>
                </div>
                <div className="font29 video_desc semibold mt15">
                  <p>Dan Vasilescu (Trupa Acum) - Colindul robului de Valeriu Gafencu</p>
                </div>
              </div>

              <div className="pl40 pr35 pt10 pb25 border_left_gray child_poem_section">
                <div className="uppercase font23 red semibold mb35">
                  <p>Alte interpretari</p>
                </div>
                <ul className="other_interpretations font19 semibold">
                  <li>Avem o țară (de Radu Gyr)- interpretează maicile de la Mănăstirea Dervent</li>
                  <li>Colind (de Radu Gyr)</li>
                  <li>Cedry2k - Ridică-te, Gheorghe, ridică-te, Ioane! de Radu Gyr</li>
                  <li>Îndemn la luptă - Radu Gyr</li>
                </ul>
              </div>
            </div>

            <hr className="red_hr"/>

            <div className="comm_section relative pb25">
              <span className="red semibold font34 uppercase">Comemorari</span>
              <span className="font15 center letterspacing7">ale poetilor din<br/> inchisorile comuniste</span>
              <span className="uppercase current_month font23">luna octombrie</span>
            </div>

            <div className="all_poets pl10">
              <div className="single_poet flex_start mb35">
                <img src="/images/poet.jpg" alt="poet"/>
                <div className="single_poet_content ml20">
                  <div className="single_poet_name semibold font29 mb20">
                    Nechifor crainic
                  </div>
                  <div className="single_poet_info1 font15 red">
                    11 octombrie
                  </div>
                  <div className="single_poet_info2 font15">
                    57 de aani de la trecerea la Domnul
                  </div>
                </div>
              </div>

              <div className="single_poet flex_start mb35">
                <img src="/images/poet.jpg" alt="poet"/>
                <div className="single_poet_content ml20">
                  <div className="single_poet_name semibold font29 mb20">
                    Nechifor crainic
                  </div>
                  <div className="single_poet_info1 font15 red">
                    11 octombrie
                  </div>
                  <div className="single_poet_info2 font15">
                    57 de aani de la trecerea la Domnul
                  </div>
                </div>
              </div>

              <div className="single_poet flex_start mb35">
                <img src="/images/poet.jpg" alt="poet"/>
                <div className="single_poet_content ml20">
                  <div className="single_poet_name semibold font29 mb20">
                    Nechifor crainic
                  </div>
                  <div className="single_poet_info1 font15 red">
                    11 octombrie
                  </div>
                  <div className="single_poet_info2 font15">
                    57 de aani de la trecerea la Domnul
                  </div>
                </div>
              </div>

              <div className="single_poet flex_start mb35">
                <img src="/images/poet.jpg" alt="poet"/>
                <div className="single_poet_content ml20">
                  <div className="single_poet_name semibold font29 mb20">
                    Nechifor crainic
                  </div>
                  <div className="single_poet_info1 font15 red">
                    11 octombrie
                  </div>
                  <div className="single_poet_info2 font15">
                    57 de aani de la trecerea la Domnul
                  </div>
                </div>
              </div>

              <div className="single_poet flex_start mb35">
                <img src="/images/poet.jpg" alt="poet"/>
                <div className="single_poet_content ml20">
                  <div className="single_poet_name semibold font29 mb20">
                    Nechifor crainic
                  </div>
                  <div className="single_poet_info1 font15 red">
                    11 octombrie
                  </div>
                  <div className="single_poet_info2 font15">
                    57 de aani de la trecerea la Domnul
                  </div>
                </div>
              </div>
            </div>
          </div>

        </Layout>


        <style jsx>{`

        `}</style>

      </div>
  )
}
