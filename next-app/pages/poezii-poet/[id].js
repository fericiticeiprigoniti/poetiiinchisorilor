import Layout from '../../components/Structure/Layout';
import {useRouter} from 'next/router';
import Link from "next/link";
import React from "react";

let url = 'http://fcp.cpco.ro/api/personaje/list';
export const getStaticPaths = async () => {
	// let url = 'http://fcp.cpco.ro/api/personaje/list';
	const res = await fetch(url);
	const data = await res.json();
	const paths = data.data.map((el) => {
		return {
			params: {id: el.id.toString()}
		};
	});
	return {
		paths: paths,
		fallback: false
	};
};
export const getStaticProps = async (context) => {
	const id = context.params.id;
	const res = await fetch(`http://fcp.cpco.ro/api/personaje/list/?id=${id}`);

	const data = await res.json();
	return {
		props: {
			poet: data
		},
		revalidate: 60
	};
};

const Poezii =({poet})=>{

	const router = useRouter();
	const {id} = router.query;

	return <Layout>
		<div className="main_container">

			{poet.data.map((el) => {
				const {nume, id, avatar} = el;
				return (<div className="flex_start_between remove_flex768" key={id}>
					<div className="first-col-biografy">
						<div className="display-flex-se mb20">
							<img alt="Nichifor Crainic" src={avatar}/>
						</div>
						<div className="name_of_author center mb20">
							<p className="uppercase">
								<Link as="/poets/id" href="/poets/id" key={poet.id}><a>{nume}</a></Link>
							</p>
							<p className="uppercase">(Ion Dobre)</p>
						</div>
						<div className="fisa-biografica">
							<ul className="list-links" id="test">
								<li className="list-links-items"><a
									href="fisa-biografica.html">Fisa
									biografica</a></li>
								<li className="list-links-items">
									<Link href="/fisa-carcerala/id"><a className="list-links-items">Fisa
										carcerala</a></Link>
									{/*<a href="/fisa-carcerala/id">Fisa carcerala</a>*/}
								</li>
								<li className="list-links-items"><a href="itinerariu-detentie.html">Intinerariu
									detentie</a>
								</li>
								{/*<li className="list-links-items active-link-item"><a href="biografie-poet.html">Biografie</a></li>*/}
								<li className="list-links-items">
									<Link href="/biografie/id" className="list-links-items"><a>Biografie</a></Link>
								</li>

								{/*<li className="list-links-items"><a href="poezie.html">Poezii</a></li>*/}
								<li className="list-links-items">
									<Link href="/biografie/id" className="list-links-items"><a
										className={`active-href`}>Poezii</a></Link>
								</li>
							</ul>
						</div>
					</div>
					<div className="second-col-biografy">
						<div className="second-wrapper">
							<div className="flex_start_between mb30 remove_flex992">
								<div className="custom_name_of_author">
									<h2>{nume}</h2>

									<div className="sublink-result">
										<p style={{color: "var(--font-grey-2)"}}><span className="red">Toate poeziile (120) | </span> Poezii
											carcerale (58)</p>
									</div>


								</div>
							</div>

							<div className="row">
								<div className="row-flex-wrap-100 mb20">
									<div className="col-left">
										<div className="mb10">
											<p className="poem-search-title">Unde sunt cei care nu mai sunt</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Întrebat-am vântul, zburătorul, <br/>
												Bidiviu pe care-aleargă norul <br/>
												Către-albastre margini de pământ: <br/>
												Unde sunt cei care nu mai sunt?
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
									<div className="col-right">
										<div className="mb10">
											<p className="poem-search-title">Rugăciune pentru pace</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Slavă ţie, Doamne, pentru-această noapte <br/>
												Somnul meu în unda lunii s-a scăldat, <br/>
												În abisul nopţii visu-nmiresmat <br/>
												Mi-a cules miresme de naramze coapte <br/>
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="border-grey"></div>
{/*=====================*/}

							<div className="row">
								<div className="row-flex-wrap-100 mb20">
									<div className="col-left">
										<div className="mb10">
											<p className="poem-search-title">Unde sunt cei care nu mai sunt</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Întrebat-am vântul, zburătorul, <br/>
												Bidiviu pe care-aleargă norul <br/>
												Către-albastre margini de pământ: <br/>
												Unde sunt cei care nu mai sunt?
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
									<div className="col-right">
										<div className="mb10">
											<p className="poem-search-title">Rugăciune pentru pace</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Slavă ţie, Doamne, pentru-această noapte <br/>
												Somnul meu în unda lunii s-a scăldat, <br/>
												În abisul nopţii visu-nmiresmat <br/>
												Mi-a cules miresme de naramze coapte <br/>
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="border-grey"></div>
{/*=========================*/}

							<div className="row">
								<div className="row-flex-wrap-100 mb20">
									<div className="col-left">
										<div className="mb10">
											<p className="poem-search-title">Unde sunt cei care nu mai sunt</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Întrebat-am vântul, zburătorul, <br/>
												Bidiviu pe care-aleargă norul <br/>
												Către-albastre margini de pământ: <br/>
												Unde sunt cei care nu mai sunt?
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
									<div className="col-right">
										<div className="mb10">
											<p className="poem-search-title">Rugăciune pentru pace</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Slavă ţie, Doamne, pentru-această noapte <br/>
												Somnul meu în unda lunii s-a scăldat, <br/>
												În abisul nopţii visu-nmiresmat <br/>
												Mi-a cules miresme de naramze coapte <br/>
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="border-grey"></div>
							{/*=====================*/}

							<div className="row">
								<div className="row-flex-wrap-100 mb20">
									<div className="col-left">
										<div className="mb10">
											<p className="poem-search-title">Unde sunt cei care nu mai sunt</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Întrebat-am vântul, zburătorul, <br/>
												Bidiviu pe care-aleargă norul <br/>
												Către-albastre margini de pământ: <br/>
												Unde sunt cei care nu mai sunt?
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
									<div className="col-right">
										<div className="mb10">
											<p className="poem-search-title">Rugăciune pentru pace</p>
										</div>
										<div className="poem">
											<p className="mb10">
												Slavă ţie, Doamne, pentru-această noapte <br/>
												Somnul meu în unda lunii s-a scăldat, <br/>
												În abisul nopţii visu-nmiresmat <br/>
												Mi-a cules miresme de naramze coapte <br/>
											</p>
										</div>
										<div className="wrapper-div">
											<div className="read_more">
												<a href="javascript:void(0)">Citește mai departe</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="border-grey"></div>
							{/*=====================*/}

							<div className="text-center m-3">
								<ul className="custom_pagination">

									<li><a href="#"><i aria-hidden="true" className="fa fa-angle-double-left"></i></a>
									</li>

									<li><a href="#">1</a></li>

									<li><a href="#">2</a></li>

									<li><a href="#">3</a></li>

									<li><a href="#">4</a></li>

									<li className="hide_xs"><a href="#">5</a></li>

									<li className="hide_xs"><a href="#">6</a></li>

									<li className="hide_xs"><a href="#">7</a></li>

									<li className="hide_xs"><a href="#">8</a></li>

									<li><a href="#"><i aria-hidden="true" className="fa fa-angle-double-right"></i></a>
									</li>

								</ul>
							</div>


						</div>
					</div>
				</div>);

			})}
			<style jsx>{`
              .content-article {
                font-family: Georgia;
                text-align: justify;
                color: var(--font-grey-2) !important;
              }
			`}</style>
		</div>
	</Layout>;

}

export default Poezii;