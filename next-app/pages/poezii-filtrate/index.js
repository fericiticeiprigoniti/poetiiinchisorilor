import React from 'react';
import Layout from "../../components/Structure/Layout";

const PoeziiFiltrate = () => {
	return (
		<Layout>
			<div className="main_container">

				<hr className="red_hr" />
					<div className="title_cat uppercase semibold center">
						<p className="title-page">Poezii</p>
					</div>
					<div className="border-bottom-big"></div>

				<div>
					<ul className="filter-ul display-flex-sb">
						<li className="filter-item red">Filtreaza poezii</li>
						<li className="filter-item">1500 de poezii / 1000 create in detentie</li>
					</ul>
				</div>
				{/*filtre*/}
				<div className="container">
					<div className="row">
						<div className="row-flex-wrap">
							<div className="form-container">
								<form>
									<div className="form-group">
										<div>
											<input className="form-input" id="autor" name="form-input"
												   placeholder="Autor" type="text"/>
										</div>
										<div>
											<input className="form-input-simple" id="titlu" name="form-input"
												   placeholder="Titlu"
												   type="text"/>
										</div>
										<div>
											<input className="form-input-simple" id="continut" name="form-input"
												   placeholder="Continut"
												   type="text"/>
										</div>
										<div>
											<input className="form-input-simple" id="subiect" name="form-input"
												   placeholder="Subiect"
												   type="text"/>
										</div>
									</div>

								</form>
							</div>
						</div>
						<div className="row-flex-wrap">
							<div className="form-container">
								<form>
									<div className="form-group">
										<div>
											<form>
												<select className="form-input-select" id="poetry" name="country">
													<option value="au">Perioada creatiei</option>
													<option value="ca">Canada</option>
													<option value="usa">USA</option>
												</select>
											</form>
										</div>
										<div>
											<form>
												<select className="form-input-select" id="poetry-2" name="country">
													<option value="au">Specia poeziei</option>
													<option value="ca">Canada</option>
													<option value="usa">USA</option>
												</select>
											</form>
										</div>

										<div>
											<input className="form-input-simple" id="place" name="form-input"
												   placeholder="Locul creatiei"
												   type="text" />
										</div>
										<div>
											<input className="form-input-simple" id="date" name="form-input"
												   placeholder="Data creatiei"
												   type="text"/>
										</div>
									</div>
								</form>
							</div>

						</div>
						<div className="row-flex-wrap">
							<div className="form-container">
								<form>
									<div className="form-group">
										<div>
											<input className="form-input-simple" id="lyrics" name="form-input"
												   placeholder="Nr. strofe"
												   type="text"/>
										</div>
										<div>
											<input className="form-input-simple" id="variant" name="form-input"
												   placeholder="Nr. variante"
												   type="text"/>
										</div>
										<button className="reset-btn">Resetare</button>
										<br/>
											<button className="filter-btn">Filtreaza</button>
									</div>

								</form>
							</div>

						</div>
					</div>
				</div>


				<div className="display-flex red-box">
					<h3 className="red-box-title">5 Rezultate</h3>
				</div>

				<div className="container">
					<div className="row mt-1">
						<div className="row-flex-wrap">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="row-flex-wrap">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="row-flex-wrap">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>
				<div className="border-grey"></div>

				<div className="container">
					<div className="row mb-2">
						<div className="row-flex-wrap-100">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleaas (...)<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="row-flex-wrap-100">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă (...))<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>


					</div>
				</div>


			</div>
		</Layout>
);
};

export default PoeziiFiltrate;
