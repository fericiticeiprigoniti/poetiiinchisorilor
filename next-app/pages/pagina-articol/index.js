import React from 'react';
import Layout from '../../components/Structure/Layout';


const Articol = () => {
	return (
		<Layout>
			<div className="main_container">
				<div className="flex_start_between remove_flex768">
					<div className="first-col-biografy">
						<div className="display-flex-se mb20">
							<img alt="Nichifor Crainic" src="/images/events.jpg" className='author-image'/>
						</div>

						<div className="mt25 font17 mb30">
							<p><span className="lightgray">Autor:</span> Dan Tudorache</p>
							<p><span className="lightgray">Publicat pe:</span> 7 dec 2017</p>
							<p><span className="lightgray">Nr vizualizari:</span> 112</p>
						</div>

						<div className="share_btn pb15">


					<span className="p15" id="share-fcb">
						<a href="javascript:void(0);"><i className="fa fa-facebook" aria-hidden="true"></i></a>
					</span>
							<span className="p15" id="share-print">
						<a href="javascript:void(0);"><i className="fa fa-print" aria-hidden="true"></i></a>
					</span>
							<span className="p15" id="share-email">
						<a href="javascript:void(0);"><i className="fa fa-envelope" aria-hidden="true"></i></a>
					</span>

						</div>

						<hr className="darkgray_hr"/>
						<h3 className='right red bold-videoteca pr-1 pt-1 uppercase'>Din acceași categorie :</h3>
						<ul className="right article_left_side mt25 mb30">
							<li>
								<a href="index.html">
									<span>Circularea poeziilor prin închisori </span>
									<i className="fa fa-angle-double-right red" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span>Cum a fost creată și transmisă poezia în închisori </span>
									<i className="fa fa-angle-double-right red" aria-hidden="true"></i>
								</a>
							</li>
							<li>
								<a href="index.html">
									<span>Ce au reprezentat poetii si poezia lor pentru patimitorii din inchisori </span>
									<i className="fa fa-angle-double-right red" aria-hidden="true"></i>
								</a>
							</li>
						</ul>

						<hr className="darkgray_hr"/>



					</div>
					<div className="second-col-biografy mb-1" style={{borderLeft: "none"}}>
						<div className="second-wrapper" style={{borderLeft: "none"}}>
							<div className="title_single_art red font23 mb30 semibold">
								În ce condiții s-a născut poezia din închisori
							</div>
							<div className="content_single_art font17">
								<p className="content-article">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Morbi placerat, ligula a fermentum convallis, quam dolor venenatis dui, ac tincidunt
									ex metus vel nisl. Nunc nec eros lorem. In semper rhoncus lacus, vestibulum molestie
									mauris accumsan sed. Etiam in lacus elit. Maecenas feugiat, nibh non porttitor
									accumsan, velit nunc placerat felis, eu maximus nibh massa ut velit. Nullam congue
									nibh a lacus scelerisque tincidunt. Donec ut nisl vel dolor suscipit ornare et vel
									nisi. Morbi aliquam urna ut tincidunt euismod. Vestibulum lobortis molestie
									vestibulum. Aenean sit amet tempus elit. Aenean turpis turpis, dapibus in metus in,
									sagittis blandit est.</p>
								<p className="content-article">Phasellus vitae sodales turpis, a efficitur metus. Sed
									est erat, dapibus sit amet nulla vitae, congue hendrerit nunc. Donec quis faucibus
									magna, a egestas sem. Donec vel nunc vitae metus bibendum blandit vel et augue.
									Mauris iaculis vulputate est, id convallis libero pretium laoreet. Suspendisse
									potenti. Suspendisse ornare auctor enim, in molestie metus finibus non. Donec vel
									eleifend libero. In eu commodo ligula, quis molestie nisi. Fusce vel nisl dictum,
									sollicitudin tortor eget, sagittis ligula. Duis sed diam in ipsum euismod auctor
									eget vel justo. Vivamus non sollicitudin dui, sodales posuere nulla.</p>
								<p className="content-article">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Morbi placerat, ligula a fermentum convallis, quam dolor venenatis dui, ac tincidunt
									ex metus vel nisl. Nunc nec eros lorem. In semper rhoncus lacus, vestibulum molestie
									mauris accumsan sed. Etiam in lacus elit. Maecenas feugiat, nibh non porttitor
									accumsan, velit nunc placerat felis, eu maximus nibh massa ut velit. Nullam congue
									nibh a lacus scelerisque tincidunt. Donec ut nisl vel dolor suscipit ornare et vel
									nisi. Morbi aliquam urna ut tincidunt euismod. Vestibulum lobortis molestie
									vestibulum. Aenean sit amet tempus elit. Aenean turpis turpis, dapibus in metus in,
									sagittis blandit est.</p>
								<p className="content-article">Phasellus vitae sodales turpis, a efficitur metus. Sed
									est erat, dapibus sit amet nulla vitae, congue hendrerit nunc. Donec quis faucibus
									magna, a egestas sem. Donec vel nunc vitae metus bibendum blandit vel et augue.
									Mauris iaculis vulputate est, id convallis libero pretium laoreet. Suspendisse
									potenti. Suspendisse ornare auctor enim, in molestie metus finibus non. Donec vel
									eleifend libero. In eu commodo ligula, quis molestie nisi. Fusce vel nisl dictum,
									sollicitudin tortor eget, sagittis ligula. Duis sed diam in ipsum euismod auctor
									eget vel justo. Vivamus non sollicitudin dui, sodales posuere nulla.</p>
								<p className="content-article">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
									Morbi placerat, ligula a fermentum convallis, quam dolor venenatis dui, ac tincidunt
									ex metus vel nisl. Nunc nec eros lorem. In semper rhoncus lacus, vestibulum molestie
									mauris accumsan sed. Etiam in lacus elit. Maecenas feugiat, nibh non porttitor
									accumsan, velit nunc placerat felis, eu maximus nibh massa ut velit. Nullam congue
									nibh a lacus scelerisque tincidunt. Donec ut nisl vel dolor suscipit ornare et vel
									nisi. Morbi aliquam urna ut tincidunt euismod. Vestibulum lobortis molestie
									vestibulum. Aenean sit amet tempus elit. Aenean turpis turpis, dapibus in metus in,
									sagittis blandit est.</p>
								<p className="content-article">Phasellus vitae sodales turpis, a efficitur metus. Sed
									est erat, dapibus sit amet nulla vitae, congue hendrerit nunc. Donec quis faucibus
									magna, a egestas sem. Donec vel nunc vitae metus bibendum blandit vel et augue.
									Mauris iaculis vulputate est, id convallis libero pretium laoreet. Suspendisse
									potenti. Suspendisse ornare auctor enim, in molestie metus finibus non. Donec vel
									eleifend libero. In eu commodo ligula, quis molestie nisi. Fusce vel nisl dictum,
									sollicitudin tortor eget, sagittis ligula. Duis sed diam in ipsum euismod auctor
									eget vel justo. Vivamus non sollicitudin dui, sodales posuere nulla.</p>
							</div>
							<div className="source_single_art font17 mb30 semibold">
								<span className="red" style={{fontFamily:"Georgia", fontWeight: '100'}}>Sursa: </span>
								<span className='author-description'> Adrian Nicolae Petcu - Revista Rost nr. 34, decembrie 2005, pp. 54-56</span>
							</div>
						</div>
					</div>
				</div>

				<style jsx>{`
               

				`}</style>
			</div>
		</Layout>
	);
};

export default Articol;
