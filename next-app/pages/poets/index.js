import Layout from "../../components/Structure/Layout";
import Link from "next/link";
import {useRouter} from "next/router";
import React from "react";

const poets = (props) => {
let {poets} = props;
    const router = useRouter();
    const {id} = router.query;
    // ==============

    let letters = [];
    let start = 'a'.charCodeAt(0);
    let last  = 'z'.charCodeAt(0);
    for (var i = start; i <= last; ++i) {
        letters.push(String.fromCharCode(i));
    }

    const letterListItems = letters.map(function (letter) {
        console.log(letter);
        return <div key={letter}><a href={"/poeti/" + letter}>{letter}</a></div>
    });
    // console.log(letterListItems.filter((letter)=> letter === props.id));
    // ==============


    return <Layout>
        <div className="main_container">
            <hr className="red_hr"/>
            <div className="title_cat uppercase semibold center">
                <div className="title-page">poeti litera <span className="red uppercase alfabet-list display-flex"> {letterListItems}</span></div>
            </div>
            {/*<div className="alphabetic mb20">*/}
            {/*    <span className="alphabetic_span red">Indice alfabetic</span>*/}
            {/*    <ul className="alphabetic_list">*/}
            {/*        {letterListItems}*/}
            {/*    </ul>*/}
            {/*</div>*/}
            <hr className="small_hr"/>
            <div className="all_letter flex_wrap_between">
                {poets.data.map((el) => {
                    return <div className="single_poet_letter flex_start mb35 mr40 ml40" key={el.id}>
                        <img alt="poet" src='/images/poet.jpg'/>
                        <div className="single_poet_content ml20">
                            <div className="single_poet_name semibold font28 mb5">
                                {/*<Link href={`/poets/${el.id}`}><a> {el.nume}</a></Link>*/}
                                <a href={`/poets/${el.id}`}>
                                    {el.nume}
                                </a>
                            </div>
                            <div className="count_poem red">
                                {el.id} de poezii
                            </div>
                            <div className="single_poet_info1">
                                Locul nașterii: , jud. Neamț
                            </div>
                            <div className="single_poet_info2">
                                Ocupația la arestare: profesor
                            </div>
                        </div>
                    </div>
                })}
            </div>
        </div>
    </Layout>
}
export const getStaticProps = async () => {
    let url = 'http://fcp.cpco.ro/api/personaje/list';
    const res = await fetch(url);
    const data = await res.json();
    console.log(data)
    return {
        props: {
            poets: data
        },
        revalidate: 100
    }
};
export default poets;

