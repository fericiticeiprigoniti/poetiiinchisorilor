import Layout from "../../components/Structure/Layout";

// fixme este facuta de 2 ori
const tempFix = {
    fontSize: 'var(-f-standard)!important',
    color: 'var(--font-grey-2) !important'
}

const filteredPoems = () => {
    // const handleSubmit =()=>{
    //     console.log('test')
    // }
// todo trebuie refacut form
    return <Layout>
        <div className="main_container">

            <hr className="red_hr"/>
            <div className="title_cat uppercase semibold center">
                <p className="title-page">Poezii</p>
            </div>
            <div className="border-bottom-big"></div>

            <div>
                <ul className="filter-ul display-flex-sb">
                    <li className="filter-item red">Filtreaza poezii</li>
                    <li className="filter-item">1500 de poezii / 1000 create in detentie</li>
                </ul>
            </div>
            <div className="container">
                <div className="row">
                    <div className="row-flex-wrap">
                        <div className="form-container">
                            <form>
                                <div className="form-group">
                                    <div>
                                        <input className="form-input" id="autor" name="form-input"
                                               placeholder="Autor" type="text"/>
                                    </div>
                                    <div>
                                        <input className="form-input-simple" id="titlu" name="form-input"
                                               placeholder="Titlu"
                                               type="text"/>
                                    </div>
                                    <div>
                                        <input className="form-input-simple" id="continut" name="form-input"
                                               placeholder="Continut"
                                               type="text"/>
                                    </div>
                                    <div>
                                        <input className="form-input-simple" id="subiect" name="form-input"
                                               placeholder="Subiect"
                                               type="text"/>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div className="row-flex-wrap">
                        <div className="form-container">
                            <form>
                                <div className="form-group">
                                    <div>
                                        <form>
                                            <select className="form-input-select" id="poetry" name="country">
                                                <option value="au">Perioada creatiei</option>
                                                <option value="ca">Canada</option>
                                                <option value="usa">USA</option>
                                            </select>
                                        </form>
                                    </div>
                                    <div>
                                        <form>
                                            <select className="form-input-select" id="poetry-2" name="country">
                                                <option value="au">Specia poeziei</option>
                                                <option value="ca">Canada</option>
                                                <option value="usa">USA</option>
                                            </select>
                                        </form>
                                    </div>

                                    <div>
                                        <input className="form-input-simple" id="place" name="form-input"
                                               placeholder="Locul creatiei"
                                               type="text"/>
                                    </div>
                                    <div>
                                        <input className="form-input-simple" id="date" name="form-input"
                                               placeholder="Data creatiei"
                                               type="text"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="row-flex-wrap">
                        <div className="form-container">
                            <form>
                                <div className="form-group">
                                    <div>
                                        <input className="form-input-simple" id="lyrics" name="form-input"
                                               placeholder="Nr. strofe"
                                               type="text"/>
                                    </div>
                                    <div>
                                        <input className="form-input-simple" id="variant" name="form-input"
                                               placeholder="Nr. variante"
                                               type="text"/>
                                    </div>
                                    <button className="reset-btn">Resetare</button>
                                    <br/>
                                    <button className="filter-btn">Filtreaza</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div className="display-flex red-box">
                <h3 className="red-box-title">5 Rezultate</h3>
            </div>
            <div className="container">
                <div className="row mt-1">
                    <div className="row-flex-wrap">
                        <div className="result-title-container">
                            <div className="semibold mb5">
                                <p className="result-title">Unde sunt cei care nu mai sunt</p>
                            </div>
                            <div className="sublink-result">
                                <p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
                                <p>Creata in detentie | 5 septembrie 1952</p>
                                <p>Penitenciarul Jilava</p>
                            </div>
                            <div className="poem_content">
                                <p style={tempFix}>
                                    Întrebat-am vîntul, zburătorul<br/>
                                    Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
                                    Către-albastre margini de pămînt:<br/>
                                    Unde sînt cei care nu mai sînt?<br/>
                                    Unde sînt cei care nu mai sînt?
                                </p>
                                <div className="read_more">
                                    <a href="poezie.html">Citește mai departe</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row-flex-wrap">
                        <div className="result-title-container">
                            <div className="semibold mb5">
                                <p className="result-title">Unde sunt cei care nu mai sunt</p>
                            </div>
                            <div className="sublink-result">
                                <p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
                                <p>Creata in detentie | 5 septembrie 1952</p>
                                <p>Penitenciarul Jilava</p>
                            </div>
                            <div className="poem_content">
                                <p style={tempFix}>
                                    Întrebat-am vîntul, zburătorul<br/>
                                    Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
                                    Către-albastre margini de pămînt:<br/>
                                    Unde sînt cei care nu mai sînt?<br/>
                                    Unde sînt cei care nu mai sînt?
                                </p>
                                <div className="read_more">
                                    <a href="poezie.html">Citește mai departe</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row-flex-wrap">
                        <div className="result-title-container">
                            <div className="semibold mb5">
                                <p className="result-title">Unde sunt cei care nu mai sunt</p>
                            </div>
                            <div className="sublink-result">
                                <p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
                                <p>Creata in detentie | 5 septembrie 1952</p>
                                <p>Penitenciarul Jilava</p>
                            </div>
                            <div className="poem_content">
                                <p style={tempFix}>
                                    Întrebat-am vîntul, zburătorul<br/>
                                    Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
                                    Către-albastre margini de pămînt:<br/>
                                    Unde sînt cei care nu mai sînt?<br/>
                                    Unde sînt cei care nu mai sînt?
                                </p>
                                <div className="read_more">
                                    <a href="poezie.html">Citește mai departe</a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <div className="border-grey"></div>

            <div className="container">
                <div className="row mb-2">
                    <div className="row-flex-wrap-100">
                        <div className="result-title-container">
                            <div className="semibold mb5">
                                <p className="result-title">Unde sunt cei care nu mai sunt</p>
                            </div>
                            <div className="sublink-result">
                                <p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
                                <p>Creata in detentie | 5 septembrie 1952</p>
                                <p>Penitenciarul Jilava</p>
                            </div>
                            <div className="poem_content">
                                <p style={tempFix}>
                                    Întrebat-am vîntul, zburătorul<br/>
                                    Bidiviu pe care-aleaas (...)<br/>
                                    Către-albastre margini de pămînt:<br/>
                                    Unde sînt cei care nu mai sînt?<br/>
                                </p>
                                <div className="read_more">
                                    <a href="poezie.html">Citește mai departe</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row-flex-wrap-100">
                        <div className="result-title-container">
                            <div className="semibold mb5">
                                <p className="result-title">Unde sunt cei care nu mai sunt</p>
                            </div>
                            <div className="sublink-result">
                                <p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
                                <p>Creata in detentie | 5 septembrie 1952</p>
                                <p>Penitenciarul Jilava</p>
                            </div>
                            <div className="poem_content">
                                <p style={tempFix}>
                                    Întrebat-am vîntul, zburătorul<br/>
                                    Bidiviu pe care-aleargă (...))<br/>
                                    Către-albastre margini de pămînt:<br/>
                                    Unde sînt cei care nu mai sînt?<br/>
                                </p>
                                <div className="read_more">
                                    <a href="poezie.html">Citește mai departe</a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


            <style jsx>{`
              .border-bottom-big {
                border-bottom: 2px solid var(--grey-2);
              }

              .filter-ul {
                font-size: var(--f-standard);
                padding: .7rem;
                background-color: var(--btn-grey);
                letter-spacing: var(--lineSpacing-1);
                margin: 0 0 1rem 0;
              }

              .filter-item {
                color: var(--font-grey-2);
              }


              .form-container {
                width: 320px;
                max-width: 500px;
                min-width: 300px;
                font-size: var(--f-standard);
                display: block;

              }

              .form-group {
                margin: .5rem;
                padding: .5rem;
              }

              .form-input {
                width: 100%;
                display: block;
                /*background-image: url(../../images/search.jpg);*/
                background: url('https://api.iconify.design/akar-icons:search.svg') no-repeat right center / contain;
                content: url('https://api.iconify.design/akar-icons:search.svg?height=24');
                vertical-align: -0.125em;
                background-size: 1.2rem;
                /*background-repeat: no-repeat;*/
                /*background-position: right center;*/
                margin-right: 10px;
              }

              .form-input-simple {
                width: 100%;
                display: block;
                background-image: none;
              }

              .form-input-select {
                width: 100%;
                display: block;
                padding: 12px 16px;
                margin: 8px 0;
                box-sizing: border-box;
                border-radius: var(--radius);
                border: 1px solid var(--btn-grey);
                color: var(--border) !important;
                font-size: 15px;
                transition: var(--transition);
              }

              .form-input-select:focus {
                outline-width: 0;
              }

              .form-input-select:hover {
                border: 1px solid var(--red-1);
              }

              input[type=text] {
                width: 100%;
                padding: 12px 20px;
                margin: 8px 0;
                box-sizing: border-box;
                border-radius: var(--radius);
                border: 1px solid var(--btn-grey);
                font-size: 15px;
                transition: var(--transition);
              }

              input[type=text]:focus {
                background-color: var(--grey-3);
                outline-width: 0;

              }

              input[type=text]:hover {
                border: 1px solid var(--red-1);
              }

              input::placeholder {
                color: var(--border);
                font-size: 15px;
              }

              .reset-btn {
                background-color: var(--btn-grey);
                border: none;
                color: black;
                padding: .6rem 1rem;
                text-decoration: none;
                margin: 4px 2px;
                cursor: pointer;
                width: 10rem;
                transition: var(--transition);
                font-size: 17px;
              }

              .reset-btn:hover {
                background-color: var(--red-1);
                letter-spacing: var(--lineSpacing-1);
                border: none;
                color: var(--white);
                padding: .6rem 1rem;
                text-decoration: none;
                margin: 4px 2px;
                cursor: pointer;
              }

              .filter-btn {
                background-color: var(--red-1);
                border: none;
                color: var(--white);
                padding: .6rem 1rem;
                text-decoration: none;
                margin: .9rem .1rem .2rem .1rem;
                cursor: pointer;
                width: 10rem;
                transition: var(--transition);
                font-size: 17px;
              }

              .filter-btn:hover {
                background-color: var(--btn-grey);
                color: var(--black-1);
              }

              .red-box {
                padding: .5rem;
                margin: .5rem 0;
                border: 1px solid var(--red-1);
                border-radius: 2px;
              }

              .row-flex-wrap-100 {
                display: flex;
                flex-wrap: wrap;
                width: 100%;
              }

              .red-box-title {
                text-transform: uppercase;
                color: var(--black-1);
                font-weight: 700;
              }

              .result-title-container {
                padding: 1rem;
                margin: 0 1rem;
              }

              .result-title {
                font-size: 18px;
                font-weight: 900;
                color: var(--black-1);

              }

              .sublink-result {
                font-size: .9rem;
                margin: .5rem 0 1rem 0;
                color: var(--grey-2);
              }

              .sublink-result p {

              }

              .border-grey {
                background-color: var(--btn-grey);
                padding: .15rem 0;
              }

              @media screen and (max-width: 768px) {
                .result-title-container {
                  padding: .5rem;
                  margin: .5rem;
                }

                .form-input-select {
                  height: 2.5rem;
                }
              }
               @media screen and (max-width: 320px) {
                .form-container {
                width:270px;
                min-width: 250px;
                }
               }
              
            `}</style>
        </div>
    </Layout>
}

export default filteredPoems;
{/*"font-size: var(--f-standard)!important; color: var(--font-grey-2) !important"*/
}
