import Layout from "../../components/Structure/Layout";


const search =()=>{
    return <Layout>
        <div className="main_container">
            <hr className="red_hr" />
                <div className="title-start uppercase semibold center">
                    <p className="page-title">Rezultate pentru: <span className="page-title-span">"Aiud"</span></p>
                </div>
                <div>
                    <p className="result-search">In poezii <span className="result-search-span">(4 rezultate)</span></p>
                    <ul className="search-list">
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Aiudule, Aiudule !
                                </p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Aiudule, Aiudule !
                                </p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Aiudule, Aiudule !
                                </p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Aiudule, Aiudule !
                                </p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>


                    </ul>
                </div>
                <div className="border-bottom"></div>
                <div>
                    <p className="result-search">In comentariile poeziilor <span className="result-search-span">(2 rezultate)</span>
                    </p>
                    <ul className="search-list">
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet,
                                    consectetur adipisicing elit. Culpa, voluptates.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>


                    </ul>
                </div>
                <div className="border-bottom"></div>

                <div>
                    <p className="result-search">In povestea poeziilor <span
                        className="result-search-span">(1 rezultat)</span></p>
                    <ul className="search-list">
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Biografie Despa
                                    Olariu</p>
                            </div>
                        </li>


                    </ul>
                </div>
                <div className="border-bottom"></div>
                <div>
                    <p className="result-search">In articole <span className="result-search-span">(5 rezultate)</span>
                    </p>
                    <ul className="search-list">
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet,
                                    consectetur adipisicing elit. Culpa, voluptates.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>

                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet,
                                    consectetur adipisicing elit. Culpa, voluptates.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Lorem ipsum dolor
                                    sit amet.</p>
                                <p className="red search-author">de Radu Gyr</p>
                            </div>
                        </li>


                    </ul>
                </div>
                <div className="border-bottom"></div>

                <div className="mb-3">
                    <p className="result-search">In povestea poeziilor <span
                        className="result-search-span">(1 rezultat)</span></p>
                    <ul className="search-list">
                        <li className="search-list-item">
                            <div>
                                <p><i aria-hidden="true" className="fa fa-caret-right red pr-1"> </i> Biografie Despa
                                    Olariu</p>
                            </div>
                        </li>
                    </ul>
                </div>
            <div className='mb-3'></div>

            <style jsx>{`
.title-start{
    letter-spacing: 2px;
    font-size: 40px;
    display: flex;
    justify-content: flex-start;
    align-content: center;
    font-weight: bolder;
}
.page-title{
    text-transform: uppercase;
    font-size: 35px;
    margin: 10px 0 20px 0;
    display: flex;
    justify-content: flex-start;
}
.page-title-span{
    font-size: 2rem;
    text-transform: capitalize;
    font-weight: 100;
    letter-spacing: 0;
  margin-top: .2rem;
    padding-left: .5rem;
}
.result-search{
    font-size: var(--f-standard);
}
.result-search-span{
    color: var(--font-grey);
    padding-left: .5rem;
}
.search-list{
    margin: 1rem 2rem;
}
.search-list-item{
    font-size: var(--f-standard);
    margin: 1rem;
}
.search-author{
    margin-left: 1.4rem;
    margin-top: .4rem;
    padding-left: .1rem;
}
.border-bottom{
border-bottom: 1px solid var(--grey-4);
    margin: 1rem 0;
}
@media screen and (max-width: 600px){
    .page-title{
        font-size: 20px !important;
    }
    .page-title-span{
        font-size: 20px !important;
        margin: 0;
    }
    .search-list{
        margin: 0.2rem;
    }
}



            `}</style>

        </div>
    </Layout>
}

export default search;
