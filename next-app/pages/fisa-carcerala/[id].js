import React, {useState, useRef} from 'react';
import {useRouter} from "next/router";
import Layout from "../../components/Structure/Layout";
import Link from "next/link";
import {FaChevronDown} from "react-icons/fa";
// import BiDownArrow from 'react-icons/fa';

let url = 'http://fcp.cpco.ro/api/personaje/list';
// creez cele 10 pagini
export const getStaticPaths = async () => {
	// let url = 'http://fcp.cpco.ro/api/personaje/list';
	const res = await fetch(url);
	const data = await res.json();
	const paths = data.data.map((el) => {
		return {
			params: {id: el.id.toString()}
		}
	})
	return {
		paths: paths,
		fallback: false
	}
}
//  info individual
export const getStaticProps = async (context) => {
	const id = context.params.id;
	const res = await fetch(`http://fcp.cpco.ro/api/personaje/list/?id=${id}`);

	const data = await res.json();
	return {
		props: {
			poet: data
		},
		revalidate: 60
	}
}


const fisaCarceralaId = ({poet}) => {

	const router = useRouter();
	const {id} = router.query;

	const [open, setOpen] = useState(false);
	const [openCard, setOpenCard] = useState(false);


// todo momente cand se blocheaza si nu mai merg butoanele
	const openHandler = () => {
		setOpen(!open);
	}
	const openHandlerCard = () => {
		setOpenCard(!openCard);
	}


	return <Layout>
		<div className="main_container">

			{poet.data.map((el) => {
				const {nume, id, avatar} = el;
				return (<div className="flex_start_between remove_flex768" key={id}>
					<div className="first-col-biografy">
						<div className="display-flex-se mb20">
							<img alt="Nichifor Crainic" src={avatar}/>
						</div>
						<div className="name_of_author center mb20">
							<p className="uppercase">
								<Link as='/poets/id' href='/poets/id' key={poet.id}><a>{nume}</a></Link>
							</p>
							<p className="uppercase">(Ion Dobre)</p>
						</div>
						<div className="fisa-biografica">
							<ul className="list-links" id="test">
								<li className="list-links-items active-link-item"><a
									href="fisa-biografica.html">Fisa
									biografica</a></li>
								<li className="list-links-items">
									<Link href='/fisa-carcerala/id'><a className={'active-href'}>Fisa
										carcerala</a></Link>
									{/*<a href="/fisa-carcerala/id">Fisa carcerala</a>*/}
								</li>
								<li className="list-links-items"><a href="itinerariu-detentie.html">Intinerariu
									detentie</a>
								</li>
								<li className="list-links-items"><a href="biografie-poet.html">Biografie</a></li>
								<li className="list-links-items"><a href="poezie.html">Poezii</a></li>
							</ul>
						</div>
					</div>
					<div className="second-col-biografy">
						<div className="wrapper-right">
							<div className="flex_start_between mb10 remove_flex992">
								<div className="custom_name_of_author">
									<h2>{nume}</h2>
								</div>
							</div>
							<div className="carcer-title">
								<h3>Sumar represiune politica </h3>
								<p>Prizonier de razboi timp de <span> 2 ani</span> in
									perioada <span>1943-1944</span> la <span>Oranki, Manastarca </span>
								</p>
								<p>Detinut politic timp de <span>13 ani</span> in perioada <span>1947-1963</span></p>
								<p>Intemnitat la <span>Jilava, Vacaresti, Aiud, MAI</span></p>
								<p>Domiciliu obligatoriuefectuat in
									perioada <span>1963-1965</span> la <span>Rachitoasa</span>
								</p>
							</div>

							<div className="carcer-title">
								<h3>Fisa Carcerala</h3>
							</div>

							<div className="carcer">
								<ul className="display-flex-end">
									<li id="open" onClick={openHandler} className={'active-text'}>Extinde fise |</li>
									<li id="close" onClick={openHandler} className={'active-text'}> Restrange fise</li>
								</ul>
							</div>

							<div className="carcer-header">
								<div className="carcer-left">
									<ul>
										<li> 1943 |<span> Condamnare</span></li>
									</ul>
								</div>
								<div className="carcer-right">
									{/*todo actiunea deshide ambele carduri*/}

									<button className="btn-carcer" id="first-btn" onClick={openHandlerCard}>
										<FaChevronDown/>
									</button>
								</div>
							</div>
							{/*{!open && <p></p>}*/}
							{open && <div className={open || openCard ? 'active-text' : ''} id="first-text">
								<div className="hidden-content">
									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Perioada de detentie :</span> 20ianuarie
											1943-14
											august 1943 (7luni)</p>

									</div>
									<p><span>Arestat la varsta de: </span> X ani</p>
									<p><span>Arestat(a) la data de </span> 14.03.1952, <span>de catre</span> M.A.I.
										Regionala
										Pitesti </p>
									<p><span>In baza mandatului nr ...,din ..., emis de catre</span> UM 0366 Pitesti</p>

									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Statutul social la momentul arestarii </span>
										</p>
									</div>
									<p><span>Ocupatia : </span> profesor</p>
									<p><span>Starea civila : </span> casatorit</p>
									<p><span>Nume sot(ie) : </span> Aglaia</p>
									<p><span>Ultimul loc de munca :</span> Facultatea de Teologie</p>
									<p><span>Apartenenta politica conform organelor represive:</span>apolitic(in trecut
										legionara)</p>
									<p><span>Originea sociala :</span></p>
									<p><span>Averea detinuta :</span></p>
									<p><span>Domiciliu la arestare :</span> Jud. Galati,Com. Faurei, Str. Crizantemelor
										nr.3
									</p>

									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Statutul social la momentul arestarii </span>
										</p>
									</div>
									<p><span>Tip condamnare/ detentie: </span> preventiva</p>
									<p><span>Condamnat la  : </span> 15 ani de munca silnica / munca silnica pe viata
									</p>
									<p><span>Nr. sentinta si instanta de condamnare : </span> 183/14.03.1945 Tribunalul
										Militar
										Bucuresti</p>
									<p><span>Fapta de condamnare :</span> uneltire contra ordinii sociale</p>
									<p><span>Descrierea pe scurt a faptei:</span></p>
									<p><span>Incadrarea legala a faptei :</span></p>
									<p><span>Perioada desfasurari pedepsei :</span>14.03.1952 -06.06.1962</p>
									<p><span>Tip recurs :</span></p>
									<p><span>Nr. sentinta si instanta de recurs :</span></p>
									<p><span>Condamnare recurs :</span></p>

									<div className="hidden-subtitle">
										<p><span className="hidden-content-title">Intionerariu detentie </span></p>
									</div>
									<p><span>MAI, Inchisoarea C
						Jilava (P) <i className="fa fa-angle-double-right"></i> Arestul Tribunalului Militar Bucuresti
                                   Aiud (P)
                                   Vacaresti (P)
                                   Jilava (P)
                            </span></p>


									<div className="hidden-subtitle">
										<p><span className="hidden-content-title">Data iesire din detentie </span></p>
									</div>
									<p><span>Tip iesire din detentie : </span> gratiat prin decretul nr. 154 / 1963</p>

									<p><span>Data iesirii :</span> 20 august 1963</p>

								</div>
							</div>}




							{/*============*/}
							<div className="carcer-header">
								<div className="carcer-left">
									<ul>
										<li> 1943 |<span> Condamnare</span></li>
									</ul>
								</div>
								<div className="carcer-right">
									<button className="btn-carcer" id="first-btn" onClick={openHandlerCard}>
										<FaChevronDown/>
									</button>
								</div>
							</div>
							{/*{!open && <p></p>}*/}
							{open && <div className="active-text" id="first-text">
								<div className="hidden-content">
									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Perioada de detentie :</span> 20ianuarie
											1943-14
											august 1943 (7luni)</p>

									</div>
									<p><span>Arestat la varsta de: </span> X ani</p>
									<p><span>Arestat(a) la data de </span> 14.03.1952, <span>de catre</span> M.A.I.
										Regionala
										Pitesti </p>
									<p><span>In baza mandatului nr ...,din ..., emis de catre</span> UM 0366 Pitesti</p>

									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Statutul social la momentul arestarii </span>
										</p>
									</div>
									<p><span>Ocupatia : </span> profesor</p>
									<p><span>Starea civila : </span> casatorit</p>
									<p><span>Nume sot(ie) : </span> Aglaia</p>
									<p><span>Ultimul loc de munca :</span> Facultatea de Teologie</p>
									<p><span>Apartenenta politica conform organelor represive:</span>apolitic(in trecut
										legionara)</p>
									<p><span>Originea sociala :</span></p>
									<p><span>Averea detinuta :</span></p>
									<p><span>Domiciliu la arestare :</span> Jud. Galati,Com. Faurei, Str. Crizantemelor
										nr.3
									</p>

									<div className="hidden-subtitle">
										<p><span
											className="hidden-content-title">Statutul social la momentul arestarii </span>
										</p>
									</div>
									<p><span>Tip condamnare/ detentie: </span> preventiva</p>
									<p><span>Condamnat la  : </span> 15 ani de munca silnica / munca silnica pe viata
									</p>
									<p><span>Nr. sentinta si instanta de condamnare : </span> 183/14.03.1945 Tribunalul
										Militar
										Bucuresti</p>
									<p><span>Fapta de condamnare :</span> uneltire contra ordinii sociale</p>
									<p><span>Descrierea pe scurt a faptei:</span></p>
									<p><span>Incadrarea legala a faptei :</span></p>
									<p><span>Perioada desfasurari pedepsei :</span>14.03.1952 -06.06.1962</p>
									<p><span>Tip recurs :</span></p>
									<p><span>Nr. sentinta si instanta de recurs :</span></p>
									<p><span>Condamnare recurs :</span></p>

									<div className="hidden-subtitle">
										<p><span className="hidden-content-title">Intionerariu detentie </span></p>
									</div>
									<p><span>MAI, Inchisoarea C
Jilava (P) <i className="fa fa-angle-double-right"></i> Arestul Tribunalului Militar Bucuresti
                                   Aiud (P)
                                   Vacaresti (P)
                                   Jilava (P)
                            </span></p>


									<div className="hidden-subtitle">
										<p><span className="hidden-content-title">Data iesire din detentie </span></p>
									</div>
									<p><span>Tip iesire din detentie : </span> gratiat prin decretul nr. 154 / 1963</p>

									<p><span>Data iesirii :</span> 20 august 1963</p>

								</div>
							</div>}



						</div>
					</div>
				</div>)

			})}
		</div>
	</Layout>
};


export default fisaCarceralaId;
