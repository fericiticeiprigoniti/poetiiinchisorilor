import {useEffect} from "react";
import {useRouter} from "next/router";
import Link from "next/link";
import Layout from "../../components/Structure/Layout";

const index = (props) => {
    // let {poets} = props;
    console.log(props)
    const router = useRouter();
    const {id}= router.query;
    console.log({id})


    return <Layout>
        <div className="main_container">
            <hr className="red_hr"/>
            <div className="title_cat uppercase semibold center">
                <p className="title-page">poeti litera <span className="red uppercase"> ..{id}</span></p>
            </div>
            <hr className="small_hr"/>
            <div className="all_letter flex_wrap_between">
                {props.poets.map((el) => {
                    return <div className="single_poet_letter flex_start mb35 mr40 ml40" key={el.id}>
                        <img alt="poet" src='/images/poet.jpg'/>
                        <div className="single_poet_content ml20">
                            <div className="single_poet_name semibold font28 mb5">
                                <a href={`/fcp/${el.id}`}>
                                    {/*{el.name}*/}
                                    {el.nume_complet}
                                </a>
                            </div>
                            <div className="count_poem red">
                                {el.id} de poezii
                            </div>
                            <div className="single_poet_info1">
                                {/*{el.address.street}*/}
                                Locul nașterii: , jud. Neamț
                            </div>
                            <div className="single_poet_info2">
                                Ocupația la arestare: profesor
                            </div>
                        </div>
                    </div>
                })}
            </div>
        </div>
    </Layout>

}

export async function getStaticProps() {
    let url = 'http://fcp.cpco.ro/api/personaje/list';
    const res = await fetch(url);
    const obj = await res.json();
    return {
        props: {
            poets: obj.data
            // poets: obj
        }
    }
}

export default index;
// return <ul>
//     {poets.map((poet)=>{
//         let url = `/fcp/${poet.id}`;
//         return <li key={poet.id} style={{padding: '1rem', margin:"1rem"}} >
//             <Link href={url}>
//                 <a>{poet.nume_complet}</a>
//             </Link>
//
//         </li>
//     })}
// </ul>
// return <h1>tst</h1>
