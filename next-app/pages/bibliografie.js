import Layout from '../components/Structure/Layout'

export default function Bibliografie() {
  return (
      <div className="container">

        <Layout title="Poeții închisorilor">

          <div className="main_container">

            <hr className="red_hr"/>
              <div className="title_cat uppercase semibold center mb20">
                <h6 className="title-main">bibliografie: poezia carcerala </h6>
              </div>
              <hr className="small_hr"/>
                <div>
                  <ul className="text-center flex_space-evenly">
                    {/* o rezolv din js */} 
                    <li className="active"><a href="#" className="active-link">Poezii</a></li>
                    <li><a href="#" className="color-red"> Antologii</a></li>
                    <li><a href="#" className="color-red"> Critica Literala</a></li>
                  </ul>
                </div>
                <div className="container">
                  {/*        1*/}
                  <div className="row">
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/1.jpg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu :</span><a href="" className="color-red">Pătimiri
                          şi iluminări din captivitatea sovietică</a></p>
                        <p className="book-info"><span className="bold">Autor : Radu Mărculescu </span></p>
                        <p className="book-info"><span className="bold">Editura : Humanitas</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1998</span></p>
                        <p className="book-info"><span className="bold">Editia : 1 </span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220 </span></p>
                      </div>
                    </div>
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/1.jpg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu :</span><a href="" className="color-red">Pătimiri
                          şi iluminări din captivitatea sovietică</a></p>
                        <p className="book-info"><span className="bold">Autor : Radu Mărculescu </span></p>
                        <p className="book-info"><span className="bold">Editura : Humanitas</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1998 </span></p>
                        <p className="book-info"><span className="bold">Editia : 1 </span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220</span></p>
                      </div>
                    </div>
                  </div>
                  <div className="border-bottom-grey"></div>
                  {/*        2*/}
                  <div className="row">
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/2.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu :</span><a href="" className="color-red">Vifornita
                          Cea Mare</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan</span></p>
                        <p className="book-info"><span className="bold">Editura : Editura Ileana</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 2013</span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 320</span></p>
                      </div>
                    </div>
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/2.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu :</span><a href="" className="color-red">Vifornita
                          Cea Mare</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan</span></p>
                        <p className="book-info"><span className="bold">Editura : Editura Ileana</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 2013</span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 320</span></p>
                      </div>
                    </div>
                  </div>
                  <div className="border-bottom-grey"></div>
                  {/*        3*/}
                  <div className="row">
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/3.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href=""
                                                                                          className="color-red"> Drumul
                          crucii</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan </span></p>
                        <p className="book-info"><span className="bold">Editura : Tehnica</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1996</span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220</span></p>
                      </div>
                    </div>
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/3.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href=""
                                                                                          className="color-red"> Drumul
                          crucii</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan </span></p>
                        <p className="book-info"><span className="bold">Editura : Tehnica</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1996</span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220</span></p>
                      </div>
                    </div>
                  </div>
                  <div className="border-bottom-grey"></div>
                  {/*        4*/}
                  <div className="row">
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/1.jpg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href="" className="color-red">Pătimiri
                          şi iluminări din captivitatea sovietică</a></p>
                        <p className="book-info"><span className="bold">Autor :  Radu Mărculescu</span></p>
                        <p className="book-info"><span className="bold">Editura :  Humanitas</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1998 </span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220</span></p>
                      </div>
                    </div>
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="img-book">
                          <img src="/images/carti/1.jpg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href="" className="color-red">Pătimiri
                          şi iluminări din captivitatea sovietică</a></p>
                        <p className="book-info"><span className="bold">Autor :  Radu Mărculescu</span></p>
                        <p className="book-info"><span className="bold">Editura :  Humanitas</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 1998 </span></p>
                        <p className="book-info"><span className="bold">Editia : 1</span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 220</span></p>
                      </div>
                    </div>
                  </div>
                  <div className="border-bottom-grey"></div>
                  {/*        5*/}
                  <div className="row">
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="mb35">
                          <img src="/images/carti/2.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href=""
                                                                                          className="red"> Vifornita Cea
                          Mare</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan</span></p>
                        <p className="book-info"><span className="bold">Editura :  Editura Ileana</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 2013</span></p>
                        <p className="book-info"><span className="bold">Editia : 1 </span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 320 </span></p>
                      </div>
                    </div>
                    <div className="col-6 flex-align book-box">
                      <div className="col-6">
                        <div className="mb35">
                          <img src="/images/carti/2.jpeg" alt="books" className="picture-info"/>
                        </div>
                      </div>
                      <div className="col-6 about-book">
                        <p className="book-info"><span className="bold">Titlu : </span><a href=""
                                                                                          className="red"> Vifornita Cea
                          Mare</a></p>
                        <p className="book-info"><span className="bold">Autor : Parintele D. Bejan</span></p>
                        <p className="book-info"><span className="bold">Editura :  Editura Ileana</span></p>
                        <p className="book-info"><span className="bold">Oras : Bucuresti, 2013</span></p>
                        <p className="book-info"><span className="bold">Editia : 1 </span></p>
                        <p className="book-info"><span className="bold">Nr. pagini : 320 </span></p>
                      </div>
                    </div>
                  </div>
                  <div className="border-bottom-grey"></div>
                </div>
                <div className="pagination">
                  <a href="#">&laquo;</a>
                  <a href="#">1</a>
                  <a className="active" href="#">2</a>
                  <a href="#">3</a>
                  <a href="#">4</a>
                  <a href="#">5</a>
                  <a href="#">6</a>
                  <a href="#">&raquo;</a>
                </div>
          </div>

          <link rel="stylesheet" href="/css/modules/bibliografie.css"/>

        </Layout>

        <style jsx>{`

        `}</style>

      </div>
  )
}
