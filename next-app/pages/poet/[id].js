import Layout from '../../components/Structure/Layout'
import { useRouter } from 'next/router'

export default function Poet() {

    const router = useRouter()
    const { id } = router.query

    return (
        <div className="container">

            <Layout title="Poeții închisorilor"
                    description="Descriere pagina pentru google search. Recomandabil sa fie intre 50 si 160 de caractere">

                <div className="main_container">
                    <div className="flex_start_between remove_flex768">
                        <div className="first_col remove_border_right">
                            <div className="portret mb20">
                                <img src="/images/nechifor_crainic.jpg" alt="Nichifor Crainic"/>
                            </div>

                            <div className="name_of_author center mb20">
                                <p className="uppercase">Nichifor Crainic</p>
                                <p className="uppercase">(Ion Dobre)</p>
                            </div>

                            <ul className="custom_list_of_author">
                                <li><a href="biografie-poet.html">Biografie</a></li>
                                <li><a href="#">Siteza operei</a></li>
                            </ul>
                        </div>
                        <div className="second_col add_border_left">
                            <div className="custom_name_of_author max_width_full mb20">
                                <h2>Nichifor Crainic</h2>
                            </div>
                            <div className="flex_start_between set_width_45">
                                <div className="details_of_author mb15">
                                    <p><span>Data nașterii: </span><span>22 Dec.1889</span></p>
                                    <p><span>Locul nașterii: </span><span>Bulbucata, jud. Neamț</span></p>
                                    <p><span>Religia: </span><span>creștin-ortodox</span></p>
                                    <p><span>Ocupație: </span><span>poet</span></p>
                                    <p><span>Data adormirii: </span><span>20 Aug. 1972</span></p>
                                    <p><span>Locul morții: </span><span>București</span></p>
                                    <p><span>Locul înmormântării: </span><span>Mănăstirea Cernica</span></p>
                                </div>
                                <div className="details_of_author mb15">
                                    <p><span>Ocupația la arestare: </span><span>profesor</span></p>
                                    <p><span>Număr de condamnări: </span><span>2</span></p>
                                    <p><span>Condamnat la: </span><span>20 de ani </span></p>
                                    <p><span>Întemnițat timp de: </span><span>15 ani</span></p>
                                    <p><span>Întemnițat la: </span><span>Jilava, Văcăreşti, Aiud</span></p>
                                </div>
                            </div>
                            <div className="author_confess mb20">
                                <a href="#" className="custom_go_to_page"><span>Mărturii despre Nechifor Crainic</span></a>
                            </div>
                            <div className="about_auth">
                                <div className="about_auth_title mb15">
                                    <h3>Opera</h3>
                                </div>
                                <ul className="list_of_articles mb30">
                                    <li><a href="#">Cursurile de mistica: I. Teologie mistica. II. Mistica germana</a>
                                    </li>
                                    <li><a href="#">Nostalgia Paradisului</a></li>
                                    <li><a href="#">Sfintenia Implinirea Umanului </a></li>
                                    <li><a href="#">Puncte Cardinale în Haos</a></li>
                                    <li><a href="#">Icoanele vremii</a></li>
                                    <li><a href="#">Poezii - Sf. O. Iosif</a></li>
                                    <li><a href="#">Iisus - copilarie si sfintenie</a></li>
                                    <li><a href="#">Ortodoxia</a></li>
                                    <li><a href="#">Dostoievski si crestinismul rus </a></li>
                                    <li><a href="#">Ortodoxie si etnocratie</a></li>
                                    <li><a href="#">Tara de peste veac</a></li>
                                    <li><a href="#">Soim peste prapastie</a></li>
                                </ul>
                                <div className="about_auth_title mb15">
                                    <h3>Cărți despre poet</h3>
                                </div>
                                <ul className="list_of_articles mb30">
                                    <li><a href="#">Cursurile de mistica: I. Teologie mistica. II. Mistica germana</a>
                                    </li>
                                    <li><a href="#">Nostalgia Paradisului</a></li>
                                    <li><a href="#">Sfintenia Implinirea Umanului </a></li>
                                    <li><a href="#">Puncte Cardinale în Haos</a></li>
                                    <li><a href="#">Icoanele vremii</a></li>
                                    <li><a href="#">Poezii - Sf. O. Iosif</a></li>
                                    <li><a href="#">Iisus - copilarie si sfintenie</a></li>
                                    <li><a href="#">Ortodoxia</a></li>
                                    <li><a href="#">Dostoievski si crestinismul rus </a></li>
                                    <li><a href="#">Ortodoxie si etnocratie</a></li>
                                    <li><a href="#">Tara de peste veac</a></li>
                                    <li><a href="#">Soim peste prapastie</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="top_color"></div>

                    <div className="flex_start_between remove_flex768 mb35">
                        <div className="first_col">
                            <div className="lirics_title right">
                                <h2>Poezii</h2>
                            </div>
                            <div className="most_popular right mb10">
                                <p>cele mai</p>
                                <p>populare</p>
                            </div>
                            <ul className="list_of_articles right remove_column_count mb30">
                                <li><a href="#">Cântec De-adormire</a></li>
                                <li><a href="#">Ţara De Peste Veac</a></li>
                                <li><a href="#">Cântec De Toamnă</a></li>
                                <li><a href="#">Trup De Fum</a></li>
                                <li><a href="#">Amiaza</a></li>
                                <li><a href="#">Cain si Abel</a></li>
                                <li><a href="#">Copacul</a></li>
                                <li><a href="#">Cules de vii</a></li>
                                <li><a href="#">Cântec de toamna</a></li>
                                <li><a href="#">Cântecul Potirului</a></li>
                                <li><a href="#">Cântecul foamei</a></li>
                                <li><a href="#">Cântecul pământului</a></li>
                                <li><a href="#">Desmarginire</a></li>
                                <li><a href="#">Dragoste</a></li>
                                <li><a href="#">Elegie</a></li>
                                <li><a href="#">Excelsior</a></li>
                                <li><a href="#">Cântec De-adormire</a></li>
                                <li><a href="#">Ţara De Peste Veac</a></li>
                                <li><a href="#">Cântec De Toamnă</a></li>
                                <li><a href="#">Trup De Fum</a></li>
                                <li><a href="#">Amiaza</a></li>
                                <li><a href="#">Cain si Abel</a></li>
                                <li><a href="#">Copacul</a></li>
                                <li><a href="#">Cules de vii</a></li>
                                <li><a href="#">Cântec de toamna</a></li>
                                <li><a href="#">Cântecul Potirului</a></li>
                                <li><a href="#">Cântecul foamei</a></li>
                                <li><a href="#">Cântecul pământului</a></li>
                                <li><a href="#">Desmarginire</a></li>
                                <li><a href="#">Dragoste</a></li>
                                <li><a href="#">Elegie</a></li>
                                <li><a href="#">Excelsior</a></li>
                            </ul>
                        </div>
                        <div className="second_col">
                            <div className="set_on_column mt25">
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Unde sunt cei care nu mai sunt</h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            Întrebat-am vîntul, zburătorul
                                            Bidiviu pe care-aleargă norul
                                            Către-albastre margini de pămînt:
                                            Unde sînt cei care nu mai sînt?
                                            Unde sînt cei care nu mai sînt?
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Cântecul foamei </h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            De voi fi fost cândva ciorchine,
                                            Sunt azi o boală stoarsă-n teasc.
                                            În flămânzenia din mine
                                            Turnați o zeamă și rănasc.
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Milogul</h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            Cerşea la colţ un slăbănog
                                            în zdrenţe, slut şi fără dinţi
                                            neguţătorul de arginţi
                                            gândi, zãrindu-l pe milog:
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Rugăciune pentru pace </h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            Slavă ţie, Doamne, pentru-această noapte
                                            Somnul meu în unda lunii s-a scăldat,
                                            În abisul nopţii visu-nmiresmat
                                            Mi-a cules miresme de naramze coapte
                                            Slavă ţie, Doamne, pentr-această noapte.
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Noaptea învierii </h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            Din spuma de visini rasare conacul
                                            Sub nemarginitul safir instelat.
                                            Aprins de-asteptare, pandeste buimacul
                                            Strangandu-si toporul sub bratu-ncordat.
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                                <div className="poems">
                                    <div className="poem_title mb10">
                                        <h1>Cântecul potirului </h1>
                                    </div>
                                    <div className="poem_lirics">
                                        <p>
                                            Când holda tăiată de seceri fu gata
                                            Bunicul şi tata
                                            Lăsară o chită de spice în picioare
                                            Legând-o cucernic cu fir de cicoare;
                                        </p>

                                        <a href="#" className="center_to_bottom mt30">Citește mai mult</a>
                                    </div>
                                </div>
                            </div>
                            <div className="list_completed mb10">
                                <p>lista completă</p>
                            </div>
                            <ul className="custom_pagination">
                                <li><a href="#" aria-label='First page'><i className="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li className="hide_xs"><a href="#">5</a></li>
                                <li className="hide_xs"><a href="#">6</a></li>
                                <li className="hide_xs"><a href="#">7</a></li>
                                <li className="hide_xs"><a href="#">8</a></li>
                                <li><a href="#" aria-label='Last page'><i className="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </Layout>


            <style jsx>{`

            `}</style>

        </div>
    )
}
