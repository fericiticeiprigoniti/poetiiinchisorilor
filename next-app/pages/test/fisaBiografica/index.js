import React from 'react';

const FisaBiografica = (props) => {
	return (
		<div className="second-wrapper">
			<div className="flex_start_between mb10 remove_flex992">
				<div className="custom_name_of_author">
					<h2>{props.nume}</h2>
				</div>
			</div>
			<div className="row">
				<div className="row-flex-wrap-100">
					<div className="col-left">
						<p className="result-title">Data nasteri: <span className="lightgray">22 decembrie 1889</span>
						</p>
						<p className="result-title">Locul nasteri: <span className="lightgray">Bulbucata, jud. Neamt</span>
						</p>
						<p className="result-title">Nationalitate: <span
							className="lightgray">Romana</span>
						</p>
						<p className="result-title">Religie / confesiune: <span
							className="lightgray">Crestin-ortodox</span>
						</p>
						<p className="result-title">Ocupatie: <span className="lightgray">poet </span>
						</p>
					</div>
					<div className="col-right">
						<p className="result-title">Data adormiri: <span
							className="lightgray">20 agust 1972</span></p>
						<p className="result-title">Locul mortii: <span
							className="lightgray">Bucuresti</span></p>
						<p className="result-title">Locul inmormantari: <span className="lightgray">Manastirea Cernica</span>
						</p>
						<p className="result-title">Parinti: <span
							className="lightgray">Ion si Aglaia</span>
						</p>
						<p className="result-title">Copii: <span
							className="lightgray">un baiat si doua fete </span></p>
					</div>
				</div>
			</div>
			<div className="biografy-chapters">
				<p className="biografy-title">Profesiuni, functii si demnitati publice</p>
				<ul className="biografy-ul">
					<li className="biografy-links lightgray">Ministrul al Propagandei (mai 1941- iunie
						1943)
					</li>
					<li className="biografy-links lightgray">Ministrul al Artelor (3 iulie 1943- 4
						august
						1944)
					</li>
				</ul>
			</div>
			<div className="biografy-chapters">
				<p className="biografy-title">Premii, distinctii si afilieri socio-profesionale</p>
				<ul className="biografy-ul">
					<li className="biografy-links lightgray">Laureat al premiului national pentru poezie
						(1930)
					</li>
					<li className="biografy-links lightgray">Doctor honoris causa al universitatii din
						Viena
						(1940)
					</li>
					<li className="biografy-links lightgray">Membru titular al Academiei Roamane (...)
					</li>
				</ul>
			</div>


			<p className="biografy-title ">Opera</p>
			<div className="row">
				<div className="row-flex-wrap-100">
					<div className="col-left">
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>

							<p className="result-title"> Cursurile de mistica: I. Teologie mistica II.
								Mistica germana
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Nostalgia Paradisului
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Sfintenia Implinirea Umanului
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Punctele Cardinale in Haos
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Icoanele vremii
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Poezii - Sf.O.Iosif
							</p>
						</div>


					</div>
					<div className="col-right">
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Isus - copilarie si sfintenie
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Ortodoxia
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Dostoievski si crestinismul rus
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Ortodoxie si etnocratie
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Tara de peste veac
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Soim peste prapastie
							</p>
						</div>
					</div>
				</div>
			</div>

			<p className="biografy-title ">Carti despre poet</p>
			<div className="row mb-5">
				<div className="row-flex-wrap-100">
					<div className="col-left">
						<div className="biografy-work">

							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>

							<p className="result-title"> Cursurile de mistica: I. Teologie mistica II.
								Mistica germana
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Nostalgia Paradisului
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Sfintenia Implinirea Umanului
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Punctele Cardinale in Haos
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Icoanele vremii
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Poezii - Sf.O.Iosif
							</p>
						</div>

					</div>
					<div className="col-right">
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Isus - copilarie si sfintenie
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Ortodoxia
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Dostoievski si crestinismul rus
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Ortodoxie si etnocratie
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Tara de peste veac
							</p>
						</div>
						<div className="biografy-work">
							<i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
							<p className="result-title"> Soim peste prapastie
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default FisaBiografica;