import React from 'react';

const IntinerariuDetentie = (props) => {

	return (
		<div className="second-wrapper" style={{borderLeft: 'none'}}>
			<div className="flex_start_between mb30 remove_flex992">
				<div className="custom_name_of_author">
					<h2>{props.nume}</h2>
				</div>
			</div>
			<div className="carcer-title">
				<h3>Sumar represiune politica </h3>
				<p>Prizonier de razboi timp de <span> 2 ani</span> in
					perioada <span>1943-1944</span> la <span>Oranki, Manastarca </span>
				</p>
				<p>Detinut politic timp de <span>13 ani</span> in perioada <span>1947-1963</span></p>
				<p>Intemnitat la <span>Jilava, Vacaresti, Aiud, MAI</span></p>
				<p>Domiciliu obligatoriuefectuat in
					perioada <span>1963-1965</span> la <span>Rachitoasa</span></p>
			</div>


			<div className="carcer-title">
				<h3>Intinerariu detentie</h3>
			</div>
			<div className="carcer-time">
				<div className="carcer-head">
					<div className="prison-time">Perioada</div>
					<span className="prison-time-left"></span>
					<span className="prison-time-right"></span>
					<div className="prison-years">1947-1963</div>
				</div>
				<div>
					<ol>
						<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai
							1946
						</p></li>
						<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
							Transfer intermediar in vederea punerii pe rol a procesului</p></li>
						<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai
							1946
						</p></li>
						<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p>
						</li>
						<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
							1952 <br/>
							Transferat pentru tratament T.B.C.</p></li>
						<li><p><span className="red">Jilava (p) </span></p></li>
					</ol>
				</div>
				<div className="carcer-head">
					<div className="prison-time">Perioada</div>
					<span className="prison-time-left"></span>
					<span className="prison-time-right"></span>
					<div className="prison-years">1947-1963</div>
				</div>
				<div>
					<ol>
						<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai
							1946
						</p></li>
						<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
							Transfer intermediar in vederea punerii pe rol a procesului</p></li>
						<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai
							1946
						</p></li>
						<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p>
						</li>
						<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
							1952 <br/>
							Transferat pentru tratament T.B.C.</p></li>
						<li><p><span className="red">Jilava (p) </span></p></li>
					</ol>
				</div>
				<div className="carcer-head">
					<div className="prison-time">Perioada</div>
					<span className="prison-time-left"></span>
					<span className="prison-time-right"></span>
					<div className="prison-years">1947-1963</div>
				</div>
				<div>
					<ol>
						<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai
							1946
						</p></li>
						<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
							Transfer intermediar in vederea punerii pe rol a procesului</p></li>
						<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai
							1946
						</p></li>
						<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p>
						</li>
						<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
							1952 <br/>
							Transferat pentru tratament T.B.C.</p></li>
						<li><p><span className="red">Jilava (p) </span></p></li>
					</ol>
				</div>

			</div>
		</div>
	);
};

export default IntinerariuDetentie;
