import React from 'react';

const IndexBio = (props) => {
	return (
		<div className="second-wrapper">
			<div className="flex_start_between mb30 remove_flex992">
				<div className="custom_name_of_author">
					<h2>{props.nume}</h2>
				</div>
			</div>

			<div className="title_of_page mb20">
				<h2><span>Biografie</span></h2>
			</div>
			<div className="content_of_page mb15">
				<p className="content-article">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse placerat
					aliquet ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada
					fames ac turpis egestas. Mauris nibh augue, eleifend at elementum eu, cursus sit
					amet ante. Nunc a diam risus. Fusce posuere elit eget odio bibendum blandit. Donec
					lacinia nisl et quam efficitur, quis blandit lorem mattis. Nullam facilisis
					ullamcorper felis eu consectetur. Donec et luctus mauris, a sollicitudin massa.
					Phasellus aliquet nulla elit, vel faucibus lorem molestie vel. Sed ultrices in neque
					ac accumsan.
				</p>
				<p className="content-article">
					Cras laoreet, libero sit amet porttitor fringilla, neque nibh pharetra massa, eu
					gravida nulla lorem at nisl. Integer eu ex eu nulla ornare faucibus at non libero.
					Nam fringilla sodales dui, vitae tincidunt velit egestas et. In vulputate, purus vel
					ultricies viverra, felis ipsum accumsan sem, in congue enim nunc at massa. Aliquam
					commodo leo libero, a rhoncus diam rutrum vel. Morbi at enim non eros vestibulum
					rhoncus. Ut suscipit enim ut orci dictum imperdiet. Vivamus dapibus, nisl nec porta
					fringilla, dolor nisl placerat dolor, in gravida eros urna at metus. Nunc sed
					consequat enim. Donec quis ex neque. Etiam finibus scelerisque blandit. Vestibulum a
					orci sed mauris viverra interdum non et enim. Nam ullamcorper ullamcorper mauris.
					Sed faucibus elementum justo in elementum. Donec eleifend vel augue ut tempus. Nam
					vulputate, dolor et interdum volutpat, ligula massa interdum libero, sed venenatis
					diam est ut purus.
				</p>
				<p className="content-article">
					Sed iaculis turpis eget elit vestibulum, ac ultrices odio ultrices. Mauris
					consectetur odio sit amet commodo ultricies. Donec risus leo, aliquet quis lacus
					ullamcorper, varius vehicula lectus. Suspendisse sit amet neque id tortor
					ullamcorper tempus. Morbi a pretium tellus. Vivamus viverra nibh id nibh imperdiet
					fermentum. Proin sollicitudin ullamcorper magna, in vulputate libero imperdiet nec.
					Quisque nec augue quis elit mattis tempor. Nulla facilisi. Curabitur sit amet
					ultrices nibh. Curabitur et lectus nisl. Suspendisse potenti. Suspendisse luctus,
					arcu eget aliquet faucibus, ex lorem fermentum erat, at vestibulum dolor lacus sed
					justo. Mauris euismod sodales enim id viverra. Proin nec condimentum turpis. Nam et
					lorem tempus lacus facilisis accumsan non nec ipsum.
				</p>
				<p className="content-article">
					Maecenas commodo quam nisi, sed sagittis dui posuere ac. Nulla malesuada orci vel
					efficitur ornare. Proin euismod sem sapien, id auctor ex finibus nec. Vestibulum
					ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
					Vestibulum est quam, eleifend sit amet mattis finibus, cursus et turpis. Aenean
					fermentum in nulla sit amet dignissim. Vestibulum ante ipsum primis in faucibus orci
					luctus et ultrices posuere cubilia Curae; Quisque non nisi et ex malesuada faucibus.
					Vivamus vel rhoncus enim, ac accumsan nulla. Sed non velit id purus interdum aliquet
					ac at diam. Quisque interdum mattis gravida. Nunc elementum sollicitudin facilisis.
				</p>
				<p className="content-article">
					Donec a dignissim enim. Morbi eget diam et est pretium sodales. Sed at fringilla
					dui. Nulla pulvinar tempor risus, vel consequat arcu lacinia eu. Sed quis diam sit
					amet tellus sollicitudin pretium. Nunc eu justo ex. Mauris volutpat mollis sapien,
					quis eleifend ante imperdiet vitae. Pellentesque quis magna ligula.
				</p>
			</div>
			<div className="author_of_article mb30">
				<div className="name_of_author">
					<h3><span>Autor: </span><span>Adrian Nicolae Petcu</span></h3>
					<h3><span>Sursa: </span><span>Adrian Nicolae Petcu - Revista Rost nr. 34, decembrie 2005, pp. 54-56</span>
					</h3>
				</div>
			</div>

		</div>
	);
};

export default IndexBio;