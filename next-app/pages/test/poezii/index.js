import React from 'react';

const PoeziiPoet = (props) => {
	return (
		<div className="second-wrapper">
			<div className="flex_start_between mb30 remove_flex992">
				<div className="custom_name_of_author">
					<h2>{props.nume}</h2>
					<div className="sublink-result">
						<p style={{color: 'var(--font-grey-2)'}}><span className="red">Toate poeziile (120) | </span> Poezii
							carcerale (58)</p>
					</div>
				</div>
			</div>
			<div className="row">
				<div className="row-flex-wrap-100 mb20">
					<div className="col-left">
						<div className="mb10">
							<p className="poem-search-title">Unde sunt cei care nu mai sunt</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Întrebat-am vântul, zburătorul, <br />
								Bidiviu pe care-aleargă norul <br />
								Către-albastre margini de pământ: <br />
								Unde sunt cei care nu mai sunt?
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
					<div className="col-right">
						<div className="mb10">
							<p className="poem-search-title">Rugăciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Slavă ţie, Doamne, pentru-această noapte <br />
								Somnul meu în unda lunii s-a scăldat, <br />
								În abisul nopţii visu-nmiresmat <br />
								Mi-a cules miresme de naramze coapte <br />
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className="border-grey"></div>

			<div className="row">
				<div className="row-flex-wrap-100 poezii-top">
					<div className="col-left">
						<div>
							<p className="poem-search-title">Rugaciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Întrebat-am vântul, zburătorul, <br />
								Bidiviu pe care-aleargă norul <br />
								Către-albastre margini de pământ: <br />
								Unde sunt cei care nu mai sunt?
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
					<div className="col-right">
						<div>
							<p className="poem-search-title">Rugăciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Slavă ţie, Doamne, pentru-această noapte <br />
								Somnul meu în unda lunii s-a scăldat, <br />
								În abisul nopţii visu-nmiresmat <br />
								Mi-a cules miresme de naramze coapte <br />
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className="border-grey"></div>

			<div className="row">
				<div className="row-flex-wrap-100 poezii-top">
					<div className="col-left">
						<div>
							<p className="poem-search-title">Rugaciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Întrebat-am vântul, zburătorul, <br />
								Bidiviu pe care-aleargă norul <br />
								Către-albastre margini de pământ: <br />
								Unde sunt cei care nu mai sunt?
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
					<div className="col-right">
						<div>
							<p className="poem-search-title">Rugăciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Slavă ţie, Doamne, pentru-această noapte <br />
								Somnul meu în unda lunii s-a scăldat, <br />
								În abisul nopţii visu-nmiresmat <br />
								Mi-a cules miresme de naramze coapte <br />
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div className="border-grey"></div>

			<div className="row">
				<div className="row-flex-wrap-100 poezii-top">
					<div className="col-left">
						<div>
							<p className="poem-search-title">Rugaciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Întrebat-am vântul, zburătorul, <br />
								Bidiviu pe care-aleargă norul <br />
								Către-albastre margini de pământ: <br />
								Unde sunt cei care nu mai sunt?
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
					<div className="col-right">
						<div>
							<p className="poem-search-title">Rugăciune pentru pace</p>
						</div>
						<div className="poem">
							<p className="mb10">
								Slavă ţie, Doamne, pentru-această noapte <br />
								Somnul meu în unda lunii s-a scăldat, <br />
								În abisul nopţii visu-nmiresmat <br />
								Mi-a cules miresme de naramze coapte <br />
							</p>
						</div>
						<div className="wrapper-div">
							<div className="read_more">
								<a href="javascript:void(0)">Citește mai departe</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			{/*pagination*/}
			<div className="text-center m-3">
				<ul className="custom_pagination">

					<li><a href="#!"><i aria-hidden="true" className="fa fa-angle-double-left"></i></a></li>

					<li><a href="javascript:void(0)">1</a></li>

					<li><a href="javascript:void(0)">2</a></li>

					<li><a href="javascript:void(0)">3</a></li>

					<li><a href="javascript:void(0)">4</a></li>

					<li className="hide_xs"><a href="javascript:void(0)">5</a></li>

					<li className="hide_xs"><a href="javascript:void(0)">6</a></li>

					<li className="hide_xs"><a href="javascript:void(0)">7</a></li>

					<li className="hide_xs"><a href="javascript:void(0)">8</a></li>

					<li><a href="javascript:void(0)"><i aria-hidden="true" className="fa fa-angle-double-right"></i></a></li>

				</ul>
			</div>
		</div>
	);
};

export default PoeziiPoet;