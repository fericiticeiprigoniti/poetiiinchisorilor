import React, {useState, useRef} from 'react';
import {useRouter} from "next/router";
import Layout from "../../components/Structure/Layout";
import Link from "next/link";
import {FaChevronDown} from "react-icons/fa";
// import BiDownArrow from 'react-icons/fa';
import Bio from './bio/index';
import IntinerariuDetentie from "./intinerariuDetentie";
import FisaBiografica from "./fisaBiografica";
import FisaCarcerala from "./fisaCarcerala";
import PoeziiPoet from "./poezii";


let url = 'http://fcp.cpco.ro/api/personaje/list';
// creez cele 10 pagini
export const getStaticPaths = async () => {
	const res = await fetch(url);
	const data = await res.json();
	const paths = data.data.map((el) => {
		return {
			params: {id: el.id.toString()}
		};
	});
	return {
		paths: paths,
		fallback: false
	};
};
//  info individual
export const getStaticProps = async (context) => {
	const id = context.params.id;
	const res = await fetch(`http://fcp.cpco.ro/api/personaje/list/?id=${id}`);
	const data = await res.json();
	return {
		props: {
			poet: data
		},
		revalidate: 60
	};
};


const fisaCarceralaId = ({poet}) => {
	const router = useRouter();
	const {id} = router.query;

	const [fisaBiografica, setFisaBiografica] = useState(true);
	const [fisaCarcerala, setFisaCarcerala] = useState(false);
	const [intinerariuDetentie, setIntinerariuDetentie] = useState(false);
	const [biografie, setBiografie] = useState(false);
	const [poezii, setPoezii] = useState(false);
	// todo un eventual loading dc dureaza fetch
	// const [isLoading, setIsLoading]=useState(true);
	const openCard1 = () => {
		setFisaBiografica(!fisaBiografica);
		setFisaCarcerala(false);
		setIntinerariuDetentie(false);
		setBiografie(false);
		setPoezii(false);
	};
	const openCard2 = () => {
		setFisaCarcerala(true);
		setFisaBiografica(false);
		setIntinerariuDetentie(false);
		setBiografie(false);
		setPoezii(false);
	};
	const openCard3 = () => {
		setIntinerariuDetentie(true);
		setFisaBiografica(false);
		setFisaCarcerala(false);
		setBiografie(false);
		setPoezii(false);
	};
	const openCard4 = () => {
		setBiografie(true);
		setFisaBiografica(false);
		setFisaCarcerala(false);
		setIntinerariuDetentie(false);
		setPoezii(false);
	};
	const openCard5 = () => {
		setPoezii(true);
		setFisaBiografica(false);
		setFisaCarcerala(false);
		setIntinerariuDetentie(false);
		setBiografie(false);
	};

	// const [open, setOpen] = useState(false);
	// const [openCard, setOpenCard] = useState(false);



	return <Layout>
		<div className="main_container">

			{poet.data.map((el) => {
				const {nume, id, avatar} = el;
				return (<div className="flex_start_between remove_flex768" key={id}>
					<div className="first-col-biografy">
						<div className="display-flex-se mb20">
							<img alt="Nichifor Crainic" src={avatar}/>
						</div>
						<div className="name_of_author center mb20">
							<p className="uppercase">
								{/*fixme Linkul nu isi mai are rostul*/}
								<Link as="/poets/id" href="/poets/id" key={poet.id}><a>{nume}</a></Link>
							</p>
							{/*<p className="uppercase">(Ion Dobre)</p>*/}
						</div>
						<div className="fisa-biografica">
							<ul className="list-links" id="test">
								<li className="list-links-items active-link-item">
									<button className={ fisaBiografica ? `button active-href`: 'button'} onClick={openCard1}>Fisa biografica</button>
								</li>
								<li className="list-links-items">
									<button className={ fisaCarcerala ? `button active-href`: 'button'} onClick={openCard2}>Fisa carcerala</button>
								</li>
								<li className="list-links-items">
									<button className={ intinerariuDetentie ? `button active-href`: 'button'} onClick={openCard3}>Intinerariu detentie</button>
								</li>
								<li className="list-links-items">
									<button className={ biografie ? `button active-href`: 'button'} onClick={openCard4}>Biografie</button>
								</li>
								<li className="list-links-items">
									<button className={ poezii ? `button active-href`: 'button'} onClick={openCard5}>Poezii</button>
								</li>
							</ul>
						</div>
					</div>

					<div className="second-col-biografy">
						{fisaBiografica && <FisaBiografica nume={nume}/>}
						{fisaCarcerala && <FisaCarcerala nume={nume} />}
						{intinerariuDetentie && <IntinerariuDetentie nume={nume}/>}
						{biografie && <Bio nume={nume} />}
						{poezii && <PoeziiPoet nume={nume}/>}
						{/*{poezii ? <PoeziiPoet nume={nume}/> : ' '}*/}
					</div>
				</div>);
			})}

			<style jsx>{`
              .button {
                border: none;
                outline: none;
                padding: .5rem 3rem;
                background: transparent;
                cursor: pointer;
              }
			`}</style>
		</div>
	</Layout>;
};


export default fisaCarceralaId;
