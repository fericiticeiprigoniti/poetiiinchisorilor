import Head from 'next/head'
// import Layout from "../components/Structure/Layout";

function MyApp({ Component, pageProps }) {
    return <>
        <Head>
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
        </Head>
        <Component {...pageProps} />
    </>
}

// From node modules
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'font-awesome/css/font-awesome.css'

// Global CSS.  // I'll leave it here for now.
import '../public/css/style.css';
import '../public/css/responsive.css';
import '../public/css/modules/categorie-articole.css';
import '../public/css/modules/bibliografie.css';
import '../public/css/modules/color-variables.css';
import '../public/css/modules/poezie.css';
import '../public/css/modules/grid-utils.css';
import '../public/css/modules/videoteca.css';
import '../public/css/modules/fisa-carcerala.css';
import '../public/css/modules/fisa-biografica.css';
import '../public/css/modules/filtered.css';
import '../public/css/modules/poezii-poet.css';
import '../public/css/modules/tematica-poeziei-carcerale.css';
import '../public/css/modules/pagina-filtrata-poezii.css';
import '../public/css/modules/intinerariu-detentie.css';
import '../public/css/modules/pagina-articol.css';


// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);
//
//   return { ...appProps }
// }

export default MyApp
