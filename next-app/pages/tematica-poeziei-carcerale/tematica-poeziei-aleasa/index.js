import React from 'react';
import Layout from '../../../components/Structure/Layout/Layout';


const Index = () => {
	return (
		<Layout>
			<div className="main_container">
				<hr className="red_hr"/>

				<div className="title_cat uppercase semibold center">
					<p className="title-page">tematica poeziei carcerale</p>
				</div>

				<hr className="small_hr"/>
				<div className="alfabet  p-1">
					<ul className="alfabet-list">
						<li><a href="#!" className="active-href">a</a></li>
						<li><a href="#!">b</a></li>
						<li><a href="#!">c</a></li>
						<li><a href="#!">d</a></li>
						<li><a href="#!">e</a></li>
						<li><a href="#!">f</a></li>
						<li><a href="#!">g</a></li>
						<li><a href="#!">h</a></li>
						<li><a href="#!">i</a></li>
						<li><a href="#!">j</a></li>
						<li><a href="#!">k</a></li>
						<li><a href="#!">l</a></li>
						<li><a href="#!">m</a></li>
						<li><a href="#!">n</a></li>
						<li><a href="#!">o</a></li>
						<li><a href="#!">p</a></li>
						<li><a href="#!">q</a></li>
						<li><a href="#!">r</a></li>
						<li><a href="#!">s</a></li>
						<li><a href="#!">t</a></li>
						<li><a href="#!">ț</a></li>
						<li><a href="#!">u</a></li>
						<li><a href="#!">v</a></li>
						<li><a href="#!">x</a></li>
						<li><a href="#!">z</a></li>
					</ul>
				</div>
				<div className="chosen-poetry mt-2 mb-1">
					<h3>
						Poezii cu tema :
						<span className='red pl10'>Activitate sociala (5)</span>
					</h3>

				</div>

				{/*=-=========*/}
				<div className="container">
					<div className="row mt-1 row-flex-wrap display-flex-start">
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="border-grey"></div>

				<div className="container">
					<div className="row mt-1 row-flex-wrap display-flex-start">
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
						<div className="">
							<div className="result-title-container">
								<div className="semibold mb5">
									<p className="result-title">Unde sunt cei care nu mai sunt</p>
								</div>
								<div className="sublink-result">
									<p><span className="red">Nichifor Crainic  </span>| 6 strofe</p>
									<p>Creata in detentie | 5 septembrie 1952</p>
									<p>Penitenciarul Jilava</p>
								</div>
								<div className="poem_content">
									{/*<p style="font-size: var(--f-standard)!important; color: var(--font-grey-2) !important">*/}
									<p className='poem-lyrics'>
										Întrebat-am vîntul, zburătorul<br/>
										Bidiviu pe care-aleargă norul dsa ads dsadsa<br/>
										Către-albastre margini de pămînt:<br/>
										Unde sînt cei care nu mai sînt?<br/>
										Unde sînt cei care nu mai sînt?
									</p>
									<div className="read_more">
										<a href="poezie.html">Citește mai departe</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


			</div>
		</Layout>
	);
};

export default Index;
