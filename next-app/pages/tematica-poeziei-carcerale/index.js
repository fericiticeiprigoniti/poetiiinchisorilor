import React from 'react';
import Layout from "../../components/Structure/Layout";


const TematicaPoezieiCarcerale = () => {
	return (
		<Layout>
			<div className="main_container">
				<hr className="red_hr" />

					<div className="title_cat uppercase semibold center">
						<p className="title-page">tematica poeziei carcerale</p>
					</div>

					<hr className="small_hr" />
						<div className="alfabet  p-1">
							<ul className="alfabet-list">
								<li><a href="#!" className="active-href">a</a></li>
								<li><a href="#!">b</a></li>
								<li><a href="#!">c</a></li>
								<li><a href="#!">d</a></li>
								<li><a href="#!">e</a></li>
								<li><a href="#!">f</a></li>
								<li><a href="#!">g</a></li>
								<li><a href="#!">h</a></li>
								<li><a href="#!">i</a></li>
								<li><a href="#!">j</a></li>
								<li><a href="#!">k</a></li>
								<li><a href="#!">l</a></li>
								<li><a href="#!">m</a></li>
								<li><a href="#!">n</a></li>
								<li><a href="#!">o</a></li>
								<li><a href="#!">p</a></li>
								<li><a href="#!">q</a></li>
								<li><a href="#!">r</a></li>
								<li><a href="#!">s</a></li>
								<li><a href="#!">t</a></li>
								<li><a href="#!">ț</a></li>
								<li><a href="#!">u</a></li>
								<li><a href="#!">v</a></li>
								<li><a href="#!">x</a></li>
								<li><a href="#!">z</a></li>
							</ul>
						</div>
						<div className="poeti-letter">
							A
						</div>
						<div className="row mb-3">
							<div className="row-flex-wrap">
								<div className="columns-tematica">
									<p><a href="">activitate sociala (2)</a></p>
									<p><a href="">Adevarul-Hristor (10)</a></p>
									<p><a href="">agoniseala de bunuri (1)</a></p>
									<p><a href="">altar (1)</a></p>
								</div>
							</div>
							<div className="row-flex-wrap">
								<div className="columns-tematica">
									<p><a href="">activitate sociala (2)</a></p>
									<p><a href="">Adevarul-Hristor (10)</a></p>
									<p><a href="">agoniseala de bunuri (1)</a></p>
									<p><a href="">altar (1)</a></p>
								</div>
							</div>
							<div className="row-flex-wrap">
								<div className="columns-tematica">
									<p><a href="">activitate sociala (2)</a></p>
									<p><a href="">Adevarul-Hristor (10)</a></p>
									<p><a href="">agoniseala de bunuri (1)</a></p>
									<p><a href="">altar (1)</a></p>
								</div>
							</div>
							<div className="row-flex-wrap">
								<div className="columns-tematica">
									<p><a href="">activitate sociala (2)</a></p>
									<p><a href="">Adevarul-Hristor (10)</a></p>
									<p><a href="">agoniseala de bunuri (1)</a></p>
									<p><a href="">altar fds fds fsdf sdf sd(1)</a></p>
								</div>
							</div>

						</div>

			</div>
		</Layout>
	);
};

export default TematicaPoezieiCarcerale;