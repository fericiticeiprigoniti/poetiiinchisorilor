import Layout from '../../components/Structure/Layout';


const intinerariuDetentie = (props) => {

	return <Layout>
		<div className="main_container">
			<div className="flex-start-evenly remove_flex768">
				<div className="first-col-biografy">
					<div className="display-flex-se mb20">
						<img alt="Nichifor Crainic" src="/images/nechifor_crainic.jpg"/>
					</div>
					<div className="name_of_author center mb20">
						<p className="uppercase">Nichifor Crainic</p>
						<p className="uppercase">(Ion Dobre)</p>
					</div>
					<div className="fisa-biografica">
						<ul className="list-links" id="test">
							<li className="list-links-items"><a href="fisa-biografica.html">Fisa
								biografica</a></li>
							<li className="list-links-items"><a href="fisa-carcerala.html">Fisa carcerala</a></li>
							<li className="list-links-items"><a className="active-href" href="itinerariu-detentie.html">Intinerariu
								detentie</a></li>
							<li className="list-links-items"><a href="biografie-poet.html">Biografie</a></li>
							<li className="list-links-items"><a href="pagina-filtrata-poezii.html">Poezii</a></li>
						</ul>
					</div>

				</div>


				<div className="second-col-biografy">
					{/*aici era un border left none*/}
					<div className="second-wrapper" style={{borderLeft: 'none'}}>
						<div className="flex_start_between mb30 remove_flex992">
							<div className="custom_name_of_author">
								<h2>Nichifor Crainic</h2>
							</div>
						</div>
						<div className="carcer-title">
							<h3>Sumar represiune politica </h3>
							<p>Prizonier de razboi timp de <span> 2 ani</span> in
								perioada <span>1943-1944</span> la <span>Oranki, Manastarca </span>
							</p>
							<p>Detinut politic timp de <span>13 ani</span> in perioada <span>1947-1963</span></p>
							<p>Intemnitat la <span>Jilava, Vacaresti, Aiud, MAI</span></p>
							<p>Domiciliu obligatoriuefectuat in
								perioada <span>1963-1965</span> la <span>Rachitoasa</span></p>
						</div>


						<div className="carcer-title">
							<h3>Intinerariu detentie</h3>
						</div>
						<div className="carcer-time">
							<div className="carcer-head">
								<div className="prison-time">Perioada</div>
								<span className="prison-time-left"></span>
								<span className="prison-time-right"></span>
								<div className="prison-years">1947-1963</div>
							</div>
							<div>
								<ol>
									<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai 1946
									</p></li>
									<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
										Transfer intermediar in vederea punerii pe rol a procesului</p></li>
									<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai 1946
									</p></li>
									<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p></li>
									<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
										1952 <br/>
										Transferat pentru tratament T.B.C.</p></li>
									<li><p><span className="red">Jilava (p) </span></p></li>
								</ol>
							</div>
							<div className="carcer-head">
								<div className="prison-time">Perioada</div>
								<span className="prison-time-left"></span>
								<span className="prison-time-right"></span>
								<div className="prison-years">1947-1963</div>
							</div>
							<div>
								<ol>
									<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai 1946
									</p></li>
									<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
										Transfer intermediar in vederea punerii pe rol a procesului</p></li>
									<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai 1946
									</p></li>
									<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p></li>
									<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
										1952 <br/>
										Transferat pentru tratament T.B.C.</p></li>
									<li><p><span className="red">Jilava (p) </span></p></li>
								</ol>
							</div>
							<div className="carcer-head">
								<div className="prison-time">Perioada</div>
								<span className="prison-time-left"></span>
								<span className="prison-time-right"></span>
								<div className="prison-years">1947-1963</div>
							</div>
							<div>
								<ol>
									<li><p><span className="red">MAI, Inchisoarea C  </span> | 3 iunie 1945 - 2 mai 1946
									</p></li>
									<li><p><span className="red">Jilava (P) </span> | (Mai 1946). <br/>
										Transfer intermediar in vederea punerii pe rol a procesului</p></li>
									<li><p><span className="red">Arestul tribunalui militar bucuresti </span> | mai 1946
									</p></li>
									<li><p><span className="red">Aiud (p) </span> | 3 iunie 1945 - 2 mai 1946</p></li>
									<li><p><span className="red">Vacaresti (P) </span> |august 1952- septembrie
										1952 <br/>
										Transferat pentru tratament T.B.C.</p></li>
									<li><p><span className="red">Jilava (p) </span></p></li>
								</ol>
							</div>

						</div>
					</div>
				</div>
			</div>

			<style jsx>{`
              .fisa-biografica {
                margin: 2rem .9rem;
                font-size: var(--f-standard);
                /*letter-spacing: var(--leter-spacing);*/
                color: var(--grey-2);
                text-align: center;
              }

              .list-links-items {
                padding: .8rem 0;
                /*margin: .5rem 0;*/
                border-bottom: 1px solid var(--border);
                transition: var(--transition);
              }

              .list-links-items:hover {
                background-color: var(--btn-grey);
                color: var(--black-1);
              }

              .list-links-items a {
                color: var(--grey-2);
              }

              .list-links-items a:hover {
                color: var(--black-1) !important;
              }

              .active-link-item {
                /*background-color: var(--btn-grey);*/
                /*background-color: var(--btn-grey);*/
                font-weight: bolder !important;
              }

              .active-href {
                /*color: var(--black-1) !important;*/
                color: var(--red-1) !important;

                font-weight: bolder;
              }


              .carcer-time {
                padding: 1rem 0;
                border-left: 1px solid var(--black-1);
                margin: 0 0 6rem 0;
              }

              .carcer-head {
                display: flex;
                /*justify-content: center;*/
                /*align-content: center;*/
              }

              .prison-time {
                background-color: var(--black-1);
                color: var(--white);
                text-transform: uppercase;
                padding: .3rem .5rem .3rem 1rem;
              }

              .prison-time-left {
                border-right: 1.5rem solid transparent;
                border-top: 1.9rem solid var(--black-1);

              }

              .prison-time-right {
                border-left: 1.5rem solid transparent;
                border-bottom: 1.9rem solid var(--red-1);
                position: relative;
                margin-left: -1rem;
              }

              .prison-years {
                background-color: var(--red-1);
                color: var(--white);
                text-transform: uppercase;
                padding: .3rem .5rem .3rem 1rem;
              }

              ol li {
                display: list-item;
                list-style-position: inside;
                list-style: decimal;
                border-bottom: 1px solid #ccc;

              }

              .carcer-time ol {
                padding: 1rem;
                margin: 1rem;
                display: block;
              }

              .carcer-time ol li {
                padding: 1rem 0 .2rem 1rem;
              }

              .carcer-time ol li span {
                text-transform: uppercase;

              }

              //====carcer
              .carcer-title {
                text-transform: uppercase;
                font-weight: bolder;
                padding: .2rem 0;
              }

              .carcer-title h3 {
                padding: .8rem 0;
              }

              .carcer-title p {
                font-size: var(--f-standard);
                text-transform: none;
                padding: .3rem 0;
                color: var(--font-grey);
              }

              .carcer-title span {
                color: var(--black-1);
              }

              .carcer ul li {
                padding: .4rem 0.1rem;
                color: var(--font-grey);
              }

              .carcer-header {
                /*display: block;*/
                width: 100%;
                background-color: var(--btn-grey);
                display: flex;
                justify-content: space-between;
                align-content: center;
                margin: .5rem 0;

              }

              .carcer-left {

              }

              .carcer-left ul {
                display: flex;
                justify-content: center;
                align-content: center;
                text-transform: uppercase;
              }

              .carcer-left ul li {
                padding: .5rem .6rem;
                color: var(--red-1);
              }

              .carcer-left span {
                color: var(--black-1);
                text-transform: uppercase;
              }

			`}</style>
		</div>
	</Layout>;

};
export default intinerariuDetentie;
