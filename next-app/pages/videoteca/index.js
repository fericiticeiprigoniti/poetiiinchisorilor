import Layout from "../../components/Structure/Layout";


const Test = () =>{
    return (
        <div className="container">
            <Layout title="proba" description="test">
                <div className="main_container">
                            <hr className="red_hr" />
                        <div className="title_cat uppercase semibold center mb20">
                            <h6 className="title-main"> Videotecă </h6>
                        </div>
                        <hr className="small_hr" />
                            <div>
                                <ul className="text-center display-flex">
                                    <li className="active"><a href="#"
                                                              className="active-link color-red links-top">Cantece <span
                                        className="p15 color_blackp">|</span> </a></li>
                                    <li><a href="#" className="links-top"> Conferinte<span className="p15">|</span></a>
                                    </li>
                                    <li><a href="#" className="links-top"> Emisiuni </a></li>
                                </ul>
                            </div>

                            <div className="container">
                                <div className="row-flex-wrap">
                                    <test2 />
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img"/>
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img"/>
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img"/>
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img"/>
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                    <div className="columns">
                                        <div className="border">
                                            <div className="video-container">
                                                <iframe width="300" height="240"
                                                        src="https://www.youtube.com/embed/hF9Gr5waAJg" frameBorder="0"
                                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                        allowFullScreen className="video-link"></iframe>
                                            </div>

                                            <div className="foto-link display-flex-sb">
                                                <img
                                                    src="https://images.unsplash.com/photo-1614185538813-dbcebdc3ecfa?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
                                                    alt="" className="foto-link-img" />
                                                    <p className="video-para">Colindul robului 1</p>
                                            </div>
                                            <div className="video-about">
                                                <p className="bold">Versuri :<span className="video-title"> Valeriu Gafencu</span>
                                                </p>
                                                <p className="bold">Muzica/voce :<span className="video-music"> Dan Vasilescu(Trupa Acum)</span>
                                                </p>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>
                    <div className="m-1 display-flex-ct">
                        <div className="pagination">
                            <a href="#">&laquo;</a>
                            <a href="#">1</a>
                            <a className="active" href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#">5</a>
                            <a href="#">6</a>
                            <a href="#">&raquo;</a>
                        </div>
                    </div>
                </div>
            </Layout>

            <style jsx>{`

                  `}</style>
        </div>
    )

}
export default Test;
