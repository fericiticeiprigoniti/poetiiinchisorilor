import styles from './categorieArticole.module.css';
import Layout from "../../components/Structure/Layout";


const Test = () => {
	return (
		<Layout title="proba" description="test" id="categorie-articole">
			<div className="main_container">
				<hr className="red_hr"/>
				<div className="title_cat uppercase semibold center">
					<p className="title-page">Fenomenul poeziei carcerale</p>
				</div>
				<hr className="small_hr"/>
				<div className="container">
					<div className="row pt-1 pb-1">
						<div className="col-7">
							<h2 className="title-h2 pl-2 pt-1">In ce
								condiții s-a nascut poezia din
								inchisori</h2>
							<p className="text-para p-2 content-article">Lorem
								ipsum dolor sit amet, consectetur
								adipisicing elit. Aspernatur atque
								cupiditate expedita inventore ipsum,
								nobis nostrum, odio possimus quaerat,
								quia quidem saepe suscipit totam!
								Accusantium architecto blanditiis error
								ipsa iure non nostrum quibusdam quisquam
								voluptas. Alias, aliquam aperiam
								aspernatur id libero modi necessitatibus
								nobis nostrum optio quaerat quam, quia,
								repudiandae.</p>
							<div className="info-link">
								<a href="#"
								   className="p-2 bold-videoteca">citește
									mai departe
									<i className="fa fa-angle-double-right info-icon"
									   aria-hidden="true"/>
								</a>
							</div>
						</div>
						<div className="col-5">
							<div className="img-book">
								<img
									src="https://images.unsplash.com/photo-1528357136257-0c25517acfea?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
									alt="books"
									className="picture-info"/>
							</div>
						</div>
					</div>
					<div className="row grey-background pt-1 pb-1">
						<div className="col-7">
							<h2 className="title-h2 pl-2 pt-1">Cum a fost
								creata si transmisă poezia in
								inchisori</h2>
							<p className="text-para p-2 content-article">Lorem
								ipsum dolor sit amet, consectetur
								adipisicing elit. Delectus dolore modi
								nostrum quas ratione ut. Accusantium
								earum exercitationem in itaque nobis.
								Architecto consequatur excepturi fugiat
								fugit nostrum nulla possimus quod
								ratione sunt, tempore! Aut doloribus
								ducimus eius exercitationem facere illum
								modi placeat possimus quod, rem,
								tenetur, ullam velit vitae
								voluptatem?</p>
							<div className="info-link">
								<a href="#"
								   className="p-2 bold-videoteca">citește
									mai departe
									<i className="fa fa-angle-double-right info-icon"
									   aria-hidden="true"/>
								</a>
							</div>
						</div>
						<div className="col-5">
							<div className="img-book">
								<img
									src="https://images.unsplash.com/photo-1528357136257-0c25517acfea?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80"
									alt="books"
									className="picture-info"/>
							</div>
						</div>
					</div>
					<div className="row pt-1 pb-1">
						<div className="col-7">
							<h2 className="title-h2 pl-2 pt-1">Circularea
								poeziilor prin inchisori</h2>
							<p className="text-para p-2 content-article">Lorem
								ipsum dolor sit amet, consectetur
								adipisicing elit. Aliquid at culpa
								dolorum libero magni nesciunt nihil
								praesentium, quae sequi sunt tenetur
								ullam velit, vero! Deleniti deserunt ea
								excepturi, explicabo incidunt, ipsum
								laborum maiores mollitia odio optio,
								perspiciatis possimus praesentium
								temporibus! Ad aliquam beatae error
								exercitationem impedit nobis qui quos
								vel.</p>
							<div className="info-link">
								<a href="#"
								   className="p-2 bold-videoteca">citește
									mai departe
									<i className="fa fa-angle-double-right info-icon"
									   aria-hidden="true"/>
								</a>
							</div>

						</div>
						<div className="col-5">
							<div className="img-book">
								<img src="images/sinteza.png" alt="books" className="picture-info"/>
							</div>
						</div>
					</div>
					<div className="text-articol grey-background">
						<h2 className="title-h2 pl-2 pt-1">Ce au reprezentat
							poetii si poezia lor patimitorii din
							inchisori</h2>
						<p className="text-para p-2 content-article">Lorem ipsum
							dolor sit amet, consectetur adipisicing elit.
							Cumque debitis delectus facilis maxime numquam,
							odio ratione rem sit tempora ullam, ut vel.
							Ducimus in inventore, magnam mollitia nesciunt
							non obcaecati, odit officia perspiciatis
							praesentium sequi sint unde veritatis
							voluptatibus voluptatum. Accusamus distinctio
							dolore facilis, iure numquam officiis saepe
							temporibus. Autem!</p>
						<div className="info-link">
							<a href="#" className="p-2 bold-videoteca">citește
								mai departe
								<i className="fa fa-angle-double-right info-icon"
								   aria-hidden="true"/>
							</a>
						</div>
					</div>
					<div className="text-articol pt-1 pb-1">
						<h2 className="title-h2 pl-2 pt-1">In ce conditii s-a
							nascut poedia din inchisori</h2>
						<p className="text-para p-2 content-article">Lorem ipsum
							dolor sit amet, consectetur adipisicing elit.
							Dolor excepturi facilis impedit minus possimus
							quas quibusdam unde velit? Culpa dolores eos et
							in iure nulla qui, quod reprehenderit sapiente
							sint. Alias blanditiis iusto quas temporibus
							voluptatem. Atque cum doloribus eaque facilis,
							fugiat fugit libero odio quod ratione recusandae
							veniam veritatis.</p>
						<div className="info-link">
							<a href="#" className="p-2 bold-videoteca">citește
								mai departe
								<i className="fa fa-angle-double-right info-icon"
								   aria-hidden="true"></i>
							</a>
						</div>
					</div>
				</div>


				<div className="text-center m-3">
					<ul className="custom_pagination">

						<li><a href="#"><i className="fa fa-angle-double-left"
										   aria-hidden="true"></i></a></li>

						<li><a href="#">1</a></li>

						<li><a href="#">2</a></li>

						<li><a href="#">3</a></li>

						<li className="hide_xs"><a href="#">4</a></li>

						<li className="hide_xs"><a href="#">5</a></li>

						<li className="hide_xs"><a href="#">6</a></li>

						<li className="hide_xs"><a href="#">7</a></li>

						<li className="hide_xs"><a href="#">8</a></li>

						<li><a href="#"><i className="fa fa-angle-double-right"
										   aria-hidden="true"></i></a></li>

					</ul>
				</div>


				<style jsx>{`
                  .col-5 {
                    -ms-flex: 0 0 41.666667%;
                    flex: 0 0 41.666667%;
                    max-width: 41.666667%;
                  }

                  .col-7 {
                    -ms-flex: 0 0 58.333333%;
                    flex: 0 0 58.333333%;
                    max-width: 58.333333%;
                  }

                  .col-md-6 {
                    flex: 0 0 50%;
                    max-width: 50%;
                  }

                  .title-h2 {
                    color: var(--red-1);
                    font-weight: 900;
                  }

                  .text-para {
                    font-size: 17px;
                    color: var(--font-grey);
                    text-align: justify;
                    line-height: var(--lineHeight-3);
                  }

                  .grey-background {
                    background-color: var(--silver-3);
                  }

                  .info-icon {
                    font-size: .8rem !important;
                    color: var(--red-1);
                    //padding: 0;
                    padding: 5px;
                    //display: flex;
                    //justify-content: center;
                    //align-content: center;
                  }

                  .info-link {
                    font-size: 1rem;
                  }

                  .text-articol {
                    padding: 1rem 0;
                  }

                  .img-book {
                    display: flex;
                    justify-content: center;
                    align-content: center;
                  }

                  .picture-info {
                    height: 320px;
                    object-position: center;
                    padding: 15px 0 15px 10px;
                  }

                  .btn-header {
                    height: 36px;
                    padding-top: 17px !important;
                  }

                  .title-page {
                    text-transform: uppercase;
                    font-size: 35px;
                    margin: 0 0 10px 0;
                  }

                  .content-article {
                    font-family: Georgia;
                    text-align: justify;
                    color: var(--font-grey-2) !important;
                    line-height: var(--lineHeight-1);
                  }

                  @media (max-width: 992px) {
                    .row {
                      display: flex;
                      flex-direction: row;
                      flex-wrap: wrap;
                      align-items: center;
                      justify-content: center;
                    }

                    .col-7 {
                      font-size: 0.8rem;
                    }

                    .info-link {
                      font-size: 0.8rem;
                    }
                  }

                  @media (max-width: 768px) {
                    .picture-info {
                      height: 22rem;
                      object-position: center;
                      padding: 15px 5px;
                    }

                    .title-page {
                      margin: 0;
                    }
                  }

                  @media (max-width: 576px) {
                    .row {
                      display: flex;
                      flex-direction: column;
                      align-items: center;
                      justify-content: center;
                      width: 100%;
                      padding: 1px 0;
                    }

                    .text-para {
                      padding: 5px !important;
                      font-size: 1rem;
                      text-align: left !important;
                    }

                    .col-7 {
                      //max-width: 88.333333%;
                      max-width: 100%;
                      line-height: var(--lineHeight-2);
                    }

                    .col-5 {
                      //max-width: 88.333333%;
                      max-width: 100%;

                    }

                    .title-h2 {
                      padding: 1px 0 5px 5px !important;
                      font-size: 1.2rem;
                    }

                    .info-link {
                      font-size: 0.7rem;
                    }
					
                    .text-articol {
                      //padding: 2rem;
                    }

                    .picture-info {
                      height: 18rem;
                      object-position: center;
                      padding: 15px 5px;
                    }

                    .title-page {
                      font-size: 25px;
                      margin: 0;
                    }
                  }

                  @media (max-width: 400px) {
                    .picture-categorie {
                      max-width: 17.5rem;
                      max-height: 15rem;
                    }

                    .col-7 {
                      width: 100%;
                    }
                  }

                  @media (max-width: 320px) {
                    .picture-categorie {
                      max-width: 16rem;
                      max-height: 14rem;
                    }
                     ul li a{
                    	font-size: 10px;
                    	
                    }
                  }
				`}</style>
			</div>
		</Layout>


	);

};
export default Test;
