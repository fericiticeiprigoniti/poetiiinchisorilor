import Layout from '../../components/Structure/Layout'
import { useRouter } from 'next/router'
import AlphabeticNavigation from "../../components/Default/AlphabeticNavigation";
import {useEffect, useState} from "react";

export default function PoetiPeLitere() {
    const [poetData, setPoetData]=useState([]);
// todo un fetch cu poets error
    let url = 'http://fcp.cpco.ro/api/personaje/list';
    useEffect(()=>{
        fetch(url)
            .then(res => res.json())
            .then(data => setPoetData(data))
            .catch(err => console.log(err))
    },[]);
    console.log(poetData);
    const router = useRouter()
    const { litera } = router.query

    return (
        <div className="container">

            <Layout title="Poeții închisorilor"
                    description="Descriere pagina pentru google search. Recomandabil sa fie intre 50 si 160 de caractere">

                <div className="main_container">
                    <hr className="red_hr"/>

                        <div className="title_cat uppercase semibold center mb20">
                            <p>poeti litera <span className="red uppercase">{litera}</span></p>
                        </div>

                        <hr className="small_hr"/>

                            <div className="all_letter flex_wrap_between">

                                <div className="single_poet_letter flex_start mb35 mr40 ml40">
                                    <img src="/images/poet.jpg" layout='fill' alt="poet"/>
                                        <div className="single_poet_content ml20">
                                            <div className="single_poet_name semibold font28 mb5">
                                                Nechifor Crainic
                                            </div>
                                            <div className="count_poem red">
                                                67 de poezii
                                            </div>
                                            <div className="single_poet_info1">
                                                Locul nașterii: Bulbucata, jud. Neamț
                                            </div>
                                            <div className="single_poet_info2">
                                                Ocupația la arestare: profesor
                                            </div>
                                        </div>
                                </div>

                                <div className="single_poet_letter flex_start mb35 mr40 ml40">
                                    <img src="/images/poet.jpg" layout='fill' alt="poet"/>
                                        <div className="single_poet_content ml20">
                                            <div className="single_poet_name semibold font28 mb5">
                                                Nechifor Crainic
                                            </div>
                                            <div className="count_poem red">
                                                67 de poezii
                                            </div>
                                            <div className="single_poet_info1">
                                                Locul nașterii: Bulbucata, jud. Neamț
                                            </div>
                                            <div className="single_poet_info2">
                                                Ocupația la arestare: profesor
                                            </div>
                                        </div>
                                </div>

                            </div>

                            <AlphabeticNavigation></AlphabeticNavigation>

                </div>


            </Layout>


            <style jsx>{`

            `}</style>

        </div>
    )
}
