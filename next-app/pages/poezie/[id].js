import React, {useEffect, useState} from "react";
import Layout from '../../components/Structure/Layout';


// let url = 'https://fcp.cpco.ro/api/poezii/poezie/';
// let url = 'https://fcp.cpco.ro/api/poezii/';
let url = 'https://fcp.cpco.ro/api/';
// export const getStaticPaths = async () => {
// 	const res = await fetch('https://fcp.cpco.ro/api/poezii/poezie/1528');
// 	//!erorr 404 https://fcp.cpco.ro/api/poezii/poezie/
// 	// console.log(res);
// 	const data = await res.json();
// 	console.log(data);
//
// 	// const paths = data.data.map((el) => {
// 	// 	return {
// 	// 		params: {id: el.id.toString()}
// 	// 	};
// 	// });
// 	return {
// 		paths: {
// 			params: {id: data}
// 		},
// 		fallback: false
// 	};
// };
// export const getStaticProps = async (context) => {
// 	const id = context.params.id;
// 	// const res = await fetch(`${url}?id=${id}`);
// 	const res = await fetch(`https://fcp.cpco.ro/api/poezii/poezie/?id=${id}`);
// 	// console.log(res);
// 	const data = await res.json();
// 	// console.log(data);
// 	return {
// 		props: {
// 			poezie: data
// 		},
// 		revalidate: 60
// 	};
// };

export const getServerSideProps = async (context) => {
	const res = await fetch('https://fcp.cpco.ro/api/poezii/poezie/1528');
	// console.log(res);
	const data = await res.json();
	console.log(data);

	return {
		props: {
			poezie: data
		}
	};
};

const PoeziePoet = ({poezie}) => {
	// console.log(poezie[0].id);
	const {
		personajInfo,
		id,
		titlu,
		continut,
		carteInfo,
		comentariuInfo,
		sursaLink,
		sursaCulegator,
		perioadaCreatieInfo
	} = poezie[0];
	console.log(id, personajInfo);
	const pozieRegex = continut.replace(/[&\/\\#^+()$~%.'":*?<p><br>;{}!@]/g, '');

	return (
		<Layout>
			<div className="main_container">

				<hr className="big_gray_hr"/>

				<div className="poetry_content pl40 pr40">
					{/*fixme temporary fix width*/}
					<div className="poetry_txt" style={{width: '70%'}}>
						<div className="uppercase red semibold font15 mb15">
							<p>{personajInfo.nume} {personajInfo.prenume}</p>
						</div>
						<div className="semibold font29 mb5">
							<p>{titlu}</p>
						</div>
						<div className="sublink-poezie">
							<p>(Unde sunt cei care au plecat)</p>
						</div>
						<div className="poem_content">
							{/*fixme trebuiesc impartine strofele*/}
							{pozieRegex}
						</div>
						<div className="border-top"></div>
						<div className="source_poetry font17">
							<p className="author-title">Poezie culeasă de: <span className="author-description">Ion Anamariei, fost deținut politic.</span>
							</p>
							<p className="author-title">Sursa: <span
								className="italic author-description">{sursaLink}.{sursaCulegator}</span><span
								className="author-description">{carteInfo.edituraNume}</span>
							</p>
						</div>
						<div className="alternative_poetry">
							<p className="alternative_title">Varianta : </p>
							<ul className="alternative_ul">
								<li className="alternative_link active-href"> 1</li>
								|
								<li className="alternative_link"> 2</li>
								|
								<li className="alternative_link"> 3</li>
								|
								<li className="alternative_link"> 4</li>
								|
								<li className="alternative_link"> 5</li>
								|
								<li className="alternative_link"> 6</li>
								|
								<li className="alternative_link"> 7</li>
								|
							</ul>
						</div>
					</div>

					<div className="details_poetry relative mt40">
						<div className="absolute more_details_poetry red uppercase">
							Detalii poezie
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10 red">
								Nr,. variante poezie : 77
							</div>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								{perioadaCreatieInfo.nume}
							</div>
							<ul className="poetry_subcategory">
								<li>in temnița si lagăre</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Subiectul poeziei
							</div>
							<ul className="poetry_subcategory">
								<li>disparuți de acasă</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Subiecte secundare
							</div>
							<ul className="poetry_subcategory">
								<li>moartea</li>
								<li>invierea</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Data si locul creației
							</div>
							<ul className="poetry_subcategory">
								<li>Februarie 1967</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Ciclul poeziei
							</div>
							<ul className="poetry_subcategory">
								<li></li>
							</ul>
						</div>


						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Specia poeziei
							</div>
							<ul className="poetry_subcategory">
								<li>Poem</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Structura strofei
							</div>
							<ul className="poetry_subcategory">
								<li>distih</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Rima
							</div>
							<ul className="poetry_subcategory">
								<li>rima împerecheată</li>
							</ul>
						</div>

						<div className="structure_poetry mb20">
							<div className="poetry_subtitle uppercase semibold font15 mb10">
								Picior metric
							</div>
							<ul className="poetry_subcategory">
								<li>iamb</li>
							</ul>
						</div>

						<div className="structure_poetry">
							<div className="poetry_subtitle uppercase semibold font15">
								Numar de strofe: <span className="red">6</span>
							</div>
						</div>
						<div className="poetry_visualization">
							Număr accesări : 58
						</div>

					</div>

				</div>

				<div className="section_audio_poetry">
					<div className="audio_version_no pb10">
						<div className="audio_poetry uppercase semibold font17">
							<p>Interpretare audio <span className="silver">[varianta 1]</span></p>
						</div>

						<hr className="big_gray_hr"/>

						<div className="content_audio_version">
							<iframe frameBorder="no" height="166" scrolling="no"
									src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/269559820&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=false"
									width="100%"></iframe>
						</div>

						<hr className="big_gray_hr"/>
					</div>

					<div className="audio_version_no pb10">
						<div className="audio_poetry uppercase semibold font17">
							<p>Interpretare audio <span className="silver">[varianta 2]</span></p>
						</div>

						<hr className="big_gray_hr"/>

						<div className="content_audio_version">
							<iframe frameBorder="no" height="166" scrolling="no"
									src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/269559820&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;show_teaser=false"
									width="100%"></iframe>
						</div>

						<hr className="big_gray_hr"/>
					</div>

				</div>


				<div className="poetry_story flex_between mb35">
					<div className="poetry_story_txt">
						<div className="poetry_story_title uppercase font17 semibold">
							<p>povestea poeziei</p>
						</div>
						<div className="poetry_story_content">
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
								laudantium,
								totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
								beatae vitae
								dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit
								aut fugit,
								sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
								velit, sed
								quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
								voluptatem.
								Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit
								laboriosam, nisi ut
								aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea
								voluptate velit
								esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas
								nulla
								pariatur?"</p>
						</div>
					</div>
					<div className="poetry_story_comment">
						<div className="poetry_story_title uppercase font17 semibold">
							<p>comentariu</p>
						</div>
						<div className="poetry_story_content">
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
								laudantium,
								totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
								beatae vitae
								dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit
								aut fugit,
								sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
							<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
								velit, sed
								quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
								voluptatem.
								Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit
								laboriosam, nisi ut
								aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea
								voluptate velit
								esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas
								nulla
								pariatur?"</p>
						</div>
					</div>
				</div>

				<div className="event_title uppercase font19">
					<p>Alte poezii de nechifor crainic</p>
				</div>

				<hr className="small_border"/>


				<ul className="flex_between other_poetries mb20">
					<li className="border_left_gray">
						<div className="semibold font23 mb10">
							<p>Noaptea învierii</p>
						</div>
						<div className="red semibold font15 mb15">
							<p>creată în temnițe și lagăre</p>
						</div>
					</li>
					<li className="border_left_gray">
						<div className="semibold font23 mb10">
							<p>Cântecul potirului</p>
						</div>
						<div className="red semibold font15 mb15">
							<p>creată în temnițe și lagăre</p>
						</div>
					</li>
					<li className="border_left_gray">
						<div className="semibold font23 mb10">
							<p>Șoim peste prăpastie</p>
						</div>
						<div className="red semibold font15 mb15">
							<p>creată în deportare</p>
						</div>
					</li>
					<li className="border_left_gray">
						<div className="semibold font23 mb10">
							<p>Prolog</p>
						</div>
						<div className="red semibold font15 mb15">
							<p>creată în deportare</p>
						</div>
					</li>
					<li className="border_left_gray">
						<div className="semibold font23 mb10">
							<p>Portret</p>
						</div>
						<div className="red semibold font15 mb15">
							<p>creată în deportare</p>
						</div>
					</li>
				</ul>

				<div className="see_all_potries right font15 semibold mb35">
					<a href="javascript:void(0)">Vezi toate poeziile acestui autor</a>
					<a className="see_all_arrow" href="javascript:void(0)">
						<i aria-hidden="true" className="fa fa-long-arrow-right"></i></a>
				</div>

				<div className="single_poet flex_start mb35 btn_poet">
					<img alt="poet" src={personajInfo.avatar}/>
					<div className="single_poet_content pl50 width100">
						<div className="single_poet_name border_b_red semibold font17 pr40 pb10 mb20">
							{personajInfo.nume}
						</div>

						<div className="flex_start poet_info">
							<ul className="poet_info1 pr40 font17">
								<li><span>Nascut:</span> {personajInfo.dataNastere}</li>
								<li><span>Locul nasterii:</span> Bulbucata, Giurgiu</li>
								<li><span>Ocupatia la arestare:</span> poet, profesor, ziarist</li>
								<li><span>Intemnitat timp de:</span> 15 ani</li>
								<li><span>Intemnitat la:</span> Jilava, Vaicaresti, Aiud</li>
								<li><span>Data adormirii:</span> {personajInfo.dataAdormire}</li>
							</ul>
							<ul className="poet_info2 font17">
								<li>
									<span>Site-uri:</span>
									<p>Poezii Nechifor Crainic</p>
									<p>Citate Nechifor Crainic</p>
								</li>
							</ul>
						</div>

						<div className="poet_testimonials font15 semibold mb15 red">
							<a className="red" href="javascript:void(0)">Mărturii despre Nechifor Crainic</a>
							<a className="testimonials_arrow" href="javascript:void(0)">
								<i aria-hidden="true" className="fa fa-long-arrow-right"></i></a>
						</div>
						<button className="font18">Biografie</button>
						<button className="font18">Sinteza operei</button>
					</div>
				</div>
				<style jsx>{`
                  .sublink-poezie {
                    color: var(--font-grey);
                    padding-bottom: 20px;
                    font-size: 20px;
                  }

                  .author-title {
                    font-weight: bold;
                    font-size: 17px;
                    color: var(--red-1);
                  }

                  .author-description {
                    font-size: 17px;
                    font-weight: normal;
                    color: var(--font-grey);
                  }

                  .alternative_poetry {
                    display: flex;
                    padding: 1rem 0;
                  }

                  .alternative_title {
                    text-transform: uppercase;
                    color: var(--red-1);
                  }

                  .alternative_ul {
                    display: flex;
                    justify-content: flex-start;
                  }

                  .alternative_link {
                    padding: 0 .5rem;
                  }

                  .alternative_link:hover {
                    color: var(--red-1);
                    font-weight: bolder;
                    cursor: pointer;
                    transform: scale(1.2);
                    transition: var(--transition);
                  }

                  .poetry_visualization {
                    padding: 1rem 0;
                    color: var(--font-grey);
                  }


                  @media (max-width: 768px) {
                    .sublink-poezie {
                      font-size: 16px
                    }

                    .author-title {
                      font-size: 16px
                    }

                    .author-description {
                      font-size: 16px
                    }
                  }

                  .border-top {
                    border-top: 1px solid var(--black-1);
                    padding-top: 20px;
                  }


				`}</style>

			</div>
		</Layout>
	);
};

export default PoeziePoet;
