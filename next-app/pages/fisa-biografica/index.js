import Layout from '../../components/Structure/Layout'


const fisaBiografica =()=>{
    return <Layout>
        <div className="main_container">
            <div className="flex_start_between remove_flex768">
                <div className="first-col-biografy">
                    <div className="display-flex-se mb20">
                        <img alt="Nichifor Crainic" src="/images/nechifor_crainic.jpg" />

                    </div>
                    <div className="name_of_author center mb20">
                        <p className="uppercase">Nichifor Crainic</p>
                        <p className="uppercase">(Ion Dobre)</p>
                    </div>
                    <div className="fisa-biografica">
                        <ul className="list-links" id="test">
                            <li className="list-links-items active-link-item"><a className="active-href"
                                                                                 href="fisa-biografica.html">Fisa
                                biografica</a></li>
                            <li className="list-links-items"><a href="fisa-carcerala.html">Fisa carcerala</a></li>
                            <li className="list-links-items"><a href="itinerariu-detentie.html">Intinerariu detentie</a>
                            </li>
                            <li className="list-links-items"><a href="biografie-poet.html">Biografie</a></li>
                            <li className="list-links-items"><a href="poezie.html">Poezii</a></li>
                        </ul>
                    </div>
                </div>
                <div className="second-col-biografy">
                    <div className="second-wrapper">
                        <div className="flex_start_between mb10 remove_flex992">
                            <div className="custom_name_of_author">
                                <h2>Nichifor Crainic</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="row-flex-wrap-100">
                                <div className="col-left">
                                    <p className="result-title">Data nasteri: <span className="lightgray">22 decembrie 1889</span>
                                    </p>
                                    <p className="result-title">Locul nasteri: <span className="lightgray">Bulbucata, jud. Neamt</span>
                                    </p>
                                    <p className="result-title">Nationalitate: <span className="lightgray">Romana</span>
                                    </p>
                                    <p className="result-title">Religie / confesiune: <span
                                        className="lightgray">Crestin-ortodox</span>
                                    </p>
                                    <p className="result-title">Ocupatie: <span className="lightgray">poet </span></p>
                                </div>
                                <div className="col-right">
                                    <p className="result-title">Data adormiri: <span
                                        className="lightgray">20 agust 1972</span></p>
                                    <p className="result-title">Locul mortii: <span
                                        className="lightgray">Bucuresti</span></p>
                                    <p className="result-title">Locul inmormantari: <span className="lightgray">Manastirea Cernica</span>
                                    </p>
                                    <p className="result-title">Parinti: <span
                                        className="lightgray">Ion si Aglaia</span>
                                    </p>
                                    <p className="result-title">Copii: <span
                                        className="lightgray">un baiat si doua fete </span></p>
                                </div>
                            </div>
                        </div>
                        <div className="biografy-chapters">
                            <p className="biografy-title">Profesiuni, functii si demnitati publice</p>
                            <ul className="biografy-ul">
                                <li className="biografy-links lightgray">Ministrul al Propagandei (mai 1941- iunie
                                    1943)
                                </li>
                                <li className="biografy-links lightgray">Ministrul al Artelor (3 iulie 1943- 4 august
                                    1944)
                                </li>
                            </ul>
                        </div>
                        <div className="biografy-chapters">
                            <p className="biografy-title">Premii, distinctii si afilieri socio-profesionale</p>
                            <ul className="biografy-ul">
                                <li className="biografy-links lightgray">Laureat al premiului national pentru poezie
                                    (1930)
                                </li>
                                <li className="biografy-links lightgray">Doctor honoris causa al universitatii din Viena
                                    (1940)
                                </li>
                                <li className="biografy-links lightgray">Membru titular al Academiei Roamane (...)</li>
                            </ul>
                        </div>


                        <p className="biografy-title ">Opera</p>
                        <div className="row">
                            <div className="row-flex-wrap-100">
                                <div className="col-left">
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>

                                        <p className="result-title"> Cursurile de mistica: I. Teologie mistica II.
                                            Mistica germana
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Nostalgia Paradisului
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Sfintenia Implinirea Umanului
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Punctele Cardinale in Haos
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Icoanele vremii
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Poezii - Sf.O.Iosif
                                        </p>
                                    </div>


                                </div>
                                <div className="col-right">
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Isus - copilarie si sfintenie
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Ortodoxia
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Dostoievski si crestinismul rus
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Ortodoxie si etnocratie
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Tara de peste veac
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Soim peste prapastie
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p className="biografy-title ">Carti despre poet</p>
                        <div className="row mb-5">
                            <div className="row-flex-wrap-100">
                                <div className="col-left">
                                    <div className="biografy-work">

                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>

                                        <p className="result-title"> Cursurile de mistica: I. Teologie mistica II.
                                            Mistica germana
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Nostalgia Paradisului
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Sfintenia Implinirea Umanului
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Punctele Cardinale in Haos
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Icoanele vremii
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Poezii - Sf.O.Iosif
                                        </p>
                                    </div>

                                </div>
                                <div className="col-right">
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Isus - copilarie si sfintenie
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Ortodoxia
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Dostoievski si crestinismul rus
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Ortodoxie si etnocratie
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Tara de peste veac
                                        </p>
                                    </div>
                                    <div className="biografy-work">
                                        <i aria-hidden="true" className="fa fa-caret-right biografy-icon"></i>
                                        <p className="result-title"> Soim peste prapastie
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <style jsx>{`
.fisa-biografica {
    margin: 2rem .9rem;
    font-size: var(--f-standard);
    /*letter-spacing: var(--leter-spacing);*/
    color: var(--grey-2);
    text-align: center;
}

.list-links-items {
    padding: .8rem 0;
    /*margin: .5rem 0;*/
    border-bottom: 1px solid var(--border);
    transition: var(--transition);
}

.list-links-items:hover {
    background-color: var(--btn-grey);
    color: var(--black-1);
}

.list-links-items a{
    color: var(--grey-2);
}

.list-links-items a:hover {
    color: var(--black-1) !important;
}

.active-link-item {
    /*background-color: var(--btn-grey);*/
    /*background-color: var(--btn-grey);*/
    font-weight: bolder !important;
}

.active-href {
    /*color: var(--black-1) !important;*/
    color: var(--red-1) !important;

    font-weight: bolder;
}

/*---------------*/
.col-left{
    /*min-width: 300px;*/
    max-width: 350px;
    width: 100%;
    /*padding-left: .6rem;*/
}
.col-left p{
    font-size: var(--f-standard);
    padding: .2rem .6rem;

}
.col-right{
    /*min-width: 300px;*/
    max-width: 350px;
    width: 100%;
}
.col-right p{
    font-size: var(--f-standard);
    padding: .2rem .6rem;
}
.biografy-chapters{
    margin: 1rem 0 1.2rem 0;
}
.biografy-title{
    font-size: var(--f-normal);
    font-weight: bolder;
    margin-top: 8px;
;
}
.biografy-chapters .biografy-ul{
    margin: .5rem 0 1rem 0;
    color: var(--grey-2);
    padding: 0 1rem;
}
.biografy-links{
    padding: .1rem 0;
    margin: .2rem 0;
    list-style-type: disc;
    font-size: var(--f-standard);
}

.biografy-work{
    display: flex;
    /*justify-content: center;*/
    flex-direction: row;
    align-content: center;
    padding-left: .7rem;
    font-weight: lighter;
}
.biografy-work p{
    font-size: 16px;
    color: var(--font-grey-2);
    font-weight: lighter;
}
.biografy-icon {
    color: var(--red-1);
    padding: 5px 0 0 0;
    //font-size: 6px;
    font-size: 14px;
}
.first-col-biografy{
    box-sizing: border-box;
    max-width: 260px;
    /*padding-right: 1rem;*/
    width: 100%;
}
.second-col-biografy{
    /*padding-left: 30px;*/
    max-width: 810px;
    width: 100%;
    /*border-left: 1px solid black;*/
}
.second-wrapper{
    padding-left: 2.5rem;
    border-left: 1px solid #cdcdcd;
}

@media screen and (max-width: 768px){
    .second-wrapper{
        padding-left: .5rem;
        border-left: none;
    }
    .first-col-biografy{
        max-width: 100%;
    }
}

            `}</style>
        </div>
    </Layout>
}

export default fisaBiografica;
